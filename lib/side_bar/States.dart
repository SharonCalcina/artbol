import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class UserRepository with ChangeNotifier {
  FirebaseAuth _auth;
  User _user;
  Status _status = Status.Uninitialized;
  List<ProdModel> listaCar;
  bool _val;
  bool _notval;
  bool emailVerified;
  bool passVerified;
  List<String> images = [];

  UserRepository.instance() : _auth = FirebaseAuth.instance {
    _auth.authStateChanges().listen(_onAuthStateChanged);
    listaCar = List<ProdModel>();
    emailVerified = false;
    passVerified = false;
  }

  Status get status => _status;
  User get user => _user;
  Future<void> navigateToNewPage(Widget page, BuildContext context) async {
    Navigator.push(
        context, MaterialPageRoute(builder: (BuildContext context) => page));
  }

  List<ProdModel> get listCar {
    return listaCar;
  }

  bool get value {
    return _val;
  }

  set value(bool va) {
    _val = va;
  }

  bool get notvalue {
    return _notval;
  }

  set notvalue(bool val) {
    _notval = val;
  }

  set listCar(List<ProdModel> list) {
    listaCar = list;
  }

  Future<bool> signIn(String email, String password) async {
    try {
      final User user = (await _auth.signInWithEmailAndPassword(
              email: email, password: password))
          .user;
      if (!user.emailVerified) {
        await user.sendEmailVerification();
      }
      _user = _auth.currentUser;
      _status = Status.Authenticated;
      print(_val);
      notifyListeners();
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      return false;
    }
  }

  Future<bool> register(String _email, String _password, String _name) async {
    try {
      final User user = (await _auth.createUserWithEmailAndPassword(
              email: _email, password: _password))
          .user;
      if (user != null) {
        if (!user.emailVerified) {
          await user.sendEmailVerification();
        }
        await user.updateProfile(displayName: _name);
        _user = _auth.currentUser;
      }
      _status = Status.Authenticating;
      notifyListeners();
      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      if (e
          .toString()
          .contains('The email address is already in use by another account')) {
        print('got it');
        emailVerified = true;
      }
      print(e.toString());
      if (e.toString().contains('Password should be at least 6 characters')) {
        print('passsword got it');
        passVerified = true;
      }
      notifyListeners();
      return false;
    }
  }

  Future signOut() async {
    await _auth.signOut();
    _status = Status.Unauthenticated;
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  Future<void> _onAuthStateChanged(User firebaseUser) async {
    if (firebaseUser == null) {
      _status = Status.Unauthenticated;
      _val = false;
      _notval = true;
      listaCar.clear();
    } else {
      _user = firebaseUser;
      _status = Status.Authenticated;
      _notval = false;
      _val = true;
      
    listaCar.clear();
    }
    notifyListeners();
  }
}
