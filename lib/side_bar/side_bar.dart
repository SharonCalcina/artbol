import 'package:artbol/pages/Home.dart';
import 'package:artbol/pagesSubmenu/Offers.dart';
import 'package:artbol/pagesSubmenu/art_marmol.dart';
import 'package:artbol/pagesSubmenu/arte_ceramica.dart';
import 'package:artbol/pagesSubmenu/arte_madera.dart';
import 'package:artbol/pagesSubmenu/arte_metal.dart';
import 'package:artbol/pagesSubmenu/arte_textil.dart';
import 'package:artbol/pagesSubmenu/crear_prod.dart';
import 'package:artbol/pagesSubmenu/inst_mus.dart';
//import 'package:artbol/pagesSubmenu/crear_prod.dart';
import 'package:artbol/pagesSubmenu/joyeria.dart';
import 'package:artbol/pagesSubmenu/otros.dart';
import 'package:artbol/pagesSubmenu/vender.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import 'Register.dart';
import 'States.dart';

class SideBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => UserRepository.instance(),
        ),
        //Provider.of<SideBar>(context,listen: false),
        //ChangeNotifierProvider(create: (_) => UserRepository.instance()),
      ],
      child: Consumer(
        // ignore: missing_return
        builder: (context, UserRepository user, _) {
          switch (user.status) {
            case Status.Uninitialized:
              return HomePage(
                text: 'SideBar1',
              );

            case Status.Unauthenticated:
              return SideBar1(
                text: 'SideBar1',
              );
            case Status.Authenticating:
              return SideBar1(
                text: 'SideBar11',
              );
            case Status.Authenticated:
              return SideBar11(
                text: 'SideBar11',
                user: user.user,
              );
          }
        },
      ),
    );
  }
}

class SideBar1 extends StatefulWidget {
  final String text;
  final bool init;

  const SideBar1({Key key, @required this.text, this.init}) : super(key: key);
  @override
  _SideBar1State createState() => _SideBar1State();
}

class _SideBar1State extends State<SideBar1> {
  GlobalKey actionKey;
  bool isDropDown = false;
  double height, width, xpos, ypos;
  OverlayEntry floatingBar;
  OverlayEntry floatingScreen;
  bool homePressed;
  String userCurrent = '';

  @override
  void initState() {
    actionKey = LabeledGlobalKey(widget.text);
    super.initState();
  }

  void findDropData() {
    RenderBox rendeRox = actionKey.currentContext.findRenderObject();
    height = rendeRox.size.height;
    width = rendeRox.size.width;
    Offset offset = rendeRox.localToGlobal(Offset.zero);
    xpos = offset.dx;
    ypos = offset.dy;
  }
/*
  OverlayEntry _createFloatingBar() {
    return OverlayEntry(builder: (context) {
      return Container(
          child: Positioned(
              // top: ypos,
              //left: xpos + width,
              width: width,
              height: MediaQuery.of(context).size.height,
              //height: height * 4,
              // child: Material(
              //color: Colors.red.shade400,
              //elevation: 20,
              child: Scaffold(
                body: Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        child: BackMenu(),
                        onTap: () {
                          setState(() {
                            if (isDropDown) {
                              floatingBar.remove();
                              floatingScreen.remove();
                            }
                            isDropDown = !isDropDown;
                          });
                        },
                      ),
                      GestureDetector(
                        child: Submenu(
                          text: "Cocina",
                          color: _color1,
                        ),
                        onTap: () {
                          Submenu(
                            text: "Cocina",
                            color: Colors.red,
                          );
                          //_color1 = Colors.red;
                          setState(() {
                            if (isDropDown) {
                              floatingBar.remove();
                              floatingScreen.remove();
                              Navigator.of(context)
                                  .pushNamed(ProdCatalog.routeProd);
                            }
                            isDropDown = !isDropDown;
                          });
                        },
                      ),
                      Submenu(text: "Pared"),
                      Submenu(text: "Sala"),
                      Submenu(text: "Jardin"),
                    ],
                  ),
                ),
              )));
    });
  }

  OverlayEntry _createFloatingScreen() {
    return OverlayEntry(builder: (context) {
      return Container(
        child: Positioned(
            left: width,
            width: width,
            height: MediaQuery.of(context).size.height,
            child: Container(
              color: Colors.transparent,
            )),
      );
    });
  }*/

  @override
  Widget build(BuildContext context) {
    //final user = Provider.of<UserRepository>(context, listen: false);
    return Container(
        width: MediaQuery.of(context).size.width * 0.6,
        child: Drawer(
            child: ListView(
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(20),
              color: Colors.red.shade300,
              child: Center(
                child: Column(
                  children: <Widget>[
                    /*Container(
                      margin: EdgeInsets.only(top: 20, bottom: 10),
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://upload.wikimedia.org/wikipedia/commons/9/91/Gigi_Hadid._2015.jpg'),
                              fit: BoxFit.fill)),
                    ),*/
                    Text(
                      'Hola',
                      style: TextStyle(
                          fontFamily: 'Sans',
                          fontSize: 12,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(userCurrent,
                        style: TextStyle(
                            fontFamily: 'Sans',
                            fontSize: 12,
                            fontWeight: FontWeight.w300))
                  ],
                ),
              ),
            ),
            GestureDetector(
              child: MenuPrin2(text: "Iniciar Sesion"),
              onTap: () async {
                //context.read<Registrar>();
                /* user.navigateToNewPage(
                        Register(
                          text: 'reg',
                        ),
                        context);*/
                /*Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => Provider.value(
                              value:user.status,
                              child: Register(
                                text: 'reg',
                              ),
                            )));
                */
                /*SchedulerBinding.instance.addPostFrameCallback((_) {
                  Navigator.of(context).pushNamed(Register.routeReg);
                });*/
                //Navigator.of(context).pushNamed(Register.routeReg);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Register(
                          text: 'Reg',
                        )));
              },
            ),
            GestureDetector(
              key: actionKey,
              child: MenuPrin(
                text: "Artesanias de ceramica",
              ),
              onTap: () {
                /*setState(() {
                  if (!isDropDown) {
                    _color1 = Color.fromARGB(255, 213, 225, 221);
                    findDropData();
                    floatingBar = _createFloatingBar();
                    floatingScreen = _createFloatingScreen();
                    Overlay.of(context).insert(floatingBar);
                    Overlay.of(context).insert(floatingScreen);
                  }

                  isDropDown = !isDropDown;
                });*/

                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteCeramica();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Artesanias de Madera"),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return ArteMadera();
                  },
                ));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Artesanias de piel, cuero y textil"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteTextil();
                }));
              },
            ),
            GestureDetector(
              child:
                  MenuPrin(text: "Artesanias de marmol, piedra, yeso y vidrio"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteMarmol();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Artesanias de metal"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteMetal();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Joyeria"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Joyeria();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Instrumentos musicales"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Instrumentos();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(
                text: "Otros tipos de artesania",
              ),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Otros();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin2(text: "Ofertas del mes"),
              onTap: () {
                Navigator.of(context).pushNamed(Offers.routeOf);
              },
            ),
            GestureDetector(
              child: MenuPrin2(text: "Ayuda"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Vender();
                }));
              },
            ),
          ],
        )));
  }
}

class SideBar11 extends StatefulWidget {
  final User user;
  final String text;
  final bool init;
  const SideBar11({Key key, @required this.text, this.init, this.user})
      : super(key: key);
  @override
  _SideBar11State createState() => _SideBar11State();
}

class _SideBar11State extends State<SideBar11> {
  _SideBar11State();
  GlobalKey actionKey;
  bool isDropDown = false;
  double height, width, xpos, ypos;
  OverlayEntry floatingBar;
  OverlayEntry floatingScreen;
  bool homePressed;

  //bool _close = false;

  @override
  void initState() {
    actionKey = LabeledGlobalKey(widget.text);
    super.initState();
  }

  void findDropData() {
    RenderBox rendeRox = actionKey.currentContext.findRenderObject();
    height = rendeRox.size.height;
    width = rendeRox.size.width;
    Offset offset = rendeRox.localToGlobal(Offset.zero);
    xpos = offset.dx;
    ypos = offset.dy;
  }

/*
  OverlayEntry _createFloatingBar() {
    return OverlayEntry(builder: (context) {
      return Container(
          child: Positioned(
              width: width,
              height: MediaQuery.of(context).size.height,
              child: Scaffold(
                body: Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        child: BackMenu(),
                        onTap: () {
                          setState(() {
                            if (isDropDown) {
                              floatingBar.remove();
                              floatingScreen.remove();
                            }
                            isDropDown = !isDropDown;
                          });
                        },
                      ),
                      GestureDetector(
                        child: Submenu(
                          text: "Cocina",
                          color: _color1,
                        ),
                        onTap: () {
                          Submenu(
                            text: "Cocina",
                            color: Colors.red,
                          );
                          //_color1 = Colors.red;
                          setState(() {
                            if (isDropDown) {
                              floatingBar.remove();
                              floatingScreen.remove();
                              Navigator.of(context)
                                  .pushNamed(ProdCatalog.routeProd);
                            }
                            isDropDown = !isDropDown;
                          });
                        },
                      ),
                      Submenu(text: "Pared"),
                      Submenu(text: "Sala"),
                      Submenu(text: "Jardin"),
                    ],
                  ),
                ),
              )));
    });
  }

  OverlayEntry _createFloatingScreen() {
    return OverlayEntry(builder: (context) {
      return Container(
        child: Positioned(
            left: width,
            width: width,
            height: MediaQuery.of(context).size.height,
            child: Container(
              color: Colors.transparent,
            )),
      );
    });
  }
*/
  @override
  Widget build(BuildContext context) {
    //final screenWidth = MediaQuery.of(context).size.width;
    return Container(
        width: MediaQuery.of(context).size.width * 0.6,
        child: Drawer(
            child: ListView(
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(20),
              color: Colors.red.shade300,
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Icon(
                      Icons.account_circle,
                      size: 60,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    /*Container(
                      margin: EdgeInsets.only(top: 20, bottom: 10),
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://upload.wikimedia.org/wikipedia/commons/9/91/Gigi_Hadid._2015.jpg'),
                              fit: BoxFit.fill)),
                    ),*/
                    Text(
                      'Hola',
                      style: TextStyle(
                          fontFamily: 'Sans',
                          fontSize: 12,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w600),
                    ),
                    Text(widget.user.email,
                        style: TextStyle(
                            fontFamily: 'Sans',
                            fontSize: 12,
                            fontWeight: FontWeight.w300))
                  ],
                ),
              ),
            ),
            GestureDetector(
              child: MenuPrin2(text: "Cerrar Sesion"),
              onTap: () async {
                Provider.of<UserRepository>(context, listen: false).signOut();
                Navigator.of(context).pushNamed(HomePage.routeHome);
              },
            ),
            GestureDetector(
              key: actionKey,
              child: MenuPrin(
                text: "Artesanias de ceramica",
              ),
              onTap: () {
                /*setState(() {
                  if (!isDropDown) {
                    _color1 = Color.fromARGB(255, 213, 225, 221);
                    findDropData();
                    floatingBar = _createFloatingBar();
                    floatingScreen = _createFloatingScreen();
                    Overlay.of(context).insert(floatingBar);
                    Overlay.of(context).insert(floatingScreen);
                  }

                  isDropDown = !isDropDown;
                });*/

                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteCeramica();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Artesanias de Madera"),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return ArteMadera();
                  },
                ));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Artesanias de piel, cuero y textil"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteTextil();
                }));
              },
            ),
            GestureDetector(
              child:
                  MenuPrin(text: "Artesanias de marmol, piedra, yeso y vidrio"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteMarmol();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Artesanias de metal"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ArteMetal();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Joyeria"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Joyeria();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(text: "Instrumentos musicales"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Instrumentos();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin(
                text: "Otros tipos de artesania",
              ),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Otros();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin2(text: "Ofertas del mes"),
              onTap: () {
                Navigator.of(context).pushNamed(Offers.routeOf);
              },
            ),
            GestureDetector(
              child: MenuPrin2(text: "Ayuda"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return Vender();
                }));
              },
            ),
            GestureDetector(
              child: MenuPrin2(text: "*** Crear Productos ***"),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return CrearProductos();
                }));
              },
            ),
          ],
        )));
  }
}

class DropDown extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.red.shade400,
        elevation: 20,
        child: Column(
          children: <Widget>[
            DropDownItem(
              text: "Cocina",
              isSelected: false,
            ),
            DropDownItem(
              text: "done",
              isSelected: true,
            ),
            DropDownItem(
              text: " Cocina",
              isSelected: false,
            ),
          ],
        ));
  }
}

class DropDownItem extends StatelessWidget {
  final String text;
  final bool isSelected;
  final double height;
  const DropDownItem({Key key, this.text, this.isSelected = false, this.height})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: isSelected ? Colors.red.shade200 : Colors.red.shade400,
      padding: EdgeInsets.all(5),
      width: double.infinity,
      child: Text(
        text,
        style: TextStyle(
            fontFamily: 'Sans',
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: Colors.black),
      ),
    );
  }
}

class Artesania extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 25),
        child: Column(
          children: <Widget>[
            BackMenu(),
            Submenu(),
            Submenu(),
          ],
        ),
      ),
    );
  }
}

class Submenu extends StatelessWidget {
  final String text;
  final Color color;
  const Submenu({Key key, this.text, this.color}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5),
        color: color,
        width: double.infinity,
        child: Row(children: <Widget>[
          Text(
            text,
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Roboto',
                fontSize: 15,
                fontWeight: FontWeight.w400),
          ),
        ]));
  }
}

class BackMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(children: <Widget>[
        Icon(
          Icons.arrow_back,
          color: Colors.red,
        ),
        Container(
          width: 5,
        ),
        Text(
          "MENU PRINCIPAL",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto',
            fontSize: 18,
          ),
        ),
      ]),
    );
  }
}

class MenuPrin extends StatelessWidget {
  final String text;
  const MenuPrin({Key key, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5),
        color: Color.fromARGB(255, 213, 225, 221),
        width: double.infinity,
        child: Row(children: <Widget>[
          Flexible(
              fit: FlexFit.tight,
              child: Text(
                text,
                style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 15,
                    fontWeight: FontWeight.w400),
              )),
          //Spacer(),
          Icon(
            Icons.arrow_right,
            color: Colors.black,
          ),
        ]));
  }
}

class MenuPrin2 extends StatelessWidget {
  final String text;
  const MenuPrin2({Key key, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5),
        color: Color.fromARGB(255, 32, 34, 40),
        //rgba(32,34,40,1)
        width: double.infinity,
        child: Row(children: <Widget>[
          Text(
            text,
            style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: 15,
                fontWeight: FontWeight.w400),
          ),
        ]));
  }
}
