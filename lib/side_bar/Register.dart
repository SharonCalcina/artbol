import 'dart:async';

import 'package:artbol/pages/Home.dart';
import 'package:artbol/pagesSubmenu/artModel.dart';
import 'package:artbol/pagesSubmenu/artModel.dart';
import 'package:artbol/pagesSubmenu/firebaseArtesanos.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fancy_dialog/FancyAnimation.dart';
import 'package:fancy_dialog/FancyTheme.dart';
import 'package:fancy_dialog/fancy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';
import 'States.dart';

class M extends StatefulWidget {
  @override
  _MState createState() => _MState();
}

class _MState extends State<M> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class Register extends StatefulWidget {
  static const routeReg = 'register';
  final String text;
  const Register({Key key, @required this.text}) : super(key: key);
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  final GlobalKey<FormState> _keyForm2 = GlobalKey<FormState>();
  GlobalKey actionKey;
  double height, width;
  double xpos, ypos;
  int radioValue1;
  int radioValue11;
  int radioValue111;
  int radioValueMaterial;
  bool checkValue = false;
  bool setVisible = false;
  TextEditingController _name = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _emailSign = TextEditingController();
  TextEditingController _passwordSign = TextEditingController();
  bool _val = true;
  bool _val2 = true;
  bool verEm = false;
  bool verPass = false;
  bool change = false;
  bool form = false;
  bool usuario = true;
  bool _isInAsyncCall = false;
  int price;
  TextEditingController _nameArt = TextEditingController();
  TextEditingController _apellido = TextEditingController();
  TextEditingController _direccion = TextEditingController();
  TextEditingController _telefono = TextEditingController();
  TextEditingController _asociacion = TextEditingController();
  TextEditingController _comunidad = TextEditingController();
  TextEditingController _material = TextEditingController();
  TextEditingController _descripcion = TextEditingController();

  String name;
  String lastName;
  String direccion;
  int cell;
  String asociacion;
  String comunidad;
  String materiales;
  String descripcion;
  int radioValue2;
  bool pertenece = true;
  bool us = true;
  bool art = false;
  final _formKey = GlobalKey<FormState>();
  List<ArtModel> listaArt = List<ArtModel>();
  FirebaseArtesanos db2 = new FirebaseArtesanos();
  StreamSubscription<QuerySnapshot> artesanos;

  List<String> email = List<String>();

  @override
  void initState() {
    actionKey = LabeledGlobalKey(widget.text);
    super.initState();
    radioValue1 = 1;
    radioValue11 = 1;
    radioValue111 = 1;
    radioValue2 = 1;
    radioValueMaterial = 1;
    listaArt = new List();
    artesanos?.cancel();
    artesanos = db2.getListProd().listen((QuerySnapshot snapshot) {
      final List<ArtModel> artesanos = snapshot.docs
          .map((documentSnapshot) => ArtModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaArt = artesanos;
      });
    });
  }

  selected(int val) {
    setState(() {
      radioValue1 = val;
    });
  }

  selected1(int val) {
    setState(() {
      radioValue11 = val;
    });
  }

  selected2(int val) {
    setState(() {
      radioValue2 = val;
    });
  }

  selectedMaterial(int val) {
    setState(() {
      radioValueMaterial = val;
    });
  }

  selected11(int val) {
    setState(() {
      radioValue111 = val;
    });
  }

  selectedCheck(bool val) {
    setState(() {
      checkValue = val;
    });
  }

  bool _validarlo() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _enviar() {
    if (_validarlo()) {
      setState(() {
        _isInAsyncCall = true;
      });
      FirebaseFirestore.instance.collection('artesano').add({
        'id': _email.text.toString(),
        'name': name,
        'lastName': lastName,
        'direccion': direccion,
        'cell': cell,
        'asociacion': asociacion,
        'comunidad': comunidad,
        'materiales': materiales,
        'descripcion': descripcion
      }).then((value) => print('Error al registrar su producto'));
      _isInAsyncCall = false;
      //}
    } else {
      print('objeto no validado');
    }
  }

  @override
  void dispose() {
    _email.dispose();
    _password.dispose();
    _emailSign.dispose();
    _passwordSign.dispose();
    super.dispose();
  }

  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade300,
            title: Text(
              'ERROR DE AUTENTICACION',
              style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 12,
                  color: Colors.red.shade800),
            ),
            content: Text(
              'Usted NO ha creado una cuenta aún para este correo',
              style: TextStyle(
                  fontFamily: 'Roboto', fontSize: 12, color: Colors.black),
            ),
          );
        });
  }

  createAlertDialog2(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade300,
            title: Text(
              'ERROR DE AUTENTICACION',
              style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 12,
                  color: Colors.red.shade800),
            ),
            content: Text(
              'Usted NO se ha registrado como un ARTESANO o NO ha creado una cuenta aún',
              style: TextStyle(
                  fontFamily: 'Roboto', fontSize: 12, color: Colors.black),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    user.emailVerified = false;
    user.passVerified = false;
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: Scaffold(
          appBar: AppBar(
            flexibleSpace: Container(
                padding: EdgeInsets.only(top: 30),
                child: Image(
                    alignment: Alignment.center,
                    height: 35,
                    width: 50,
                    image: AssetImage('assets/images/logo2.png'))),
          ),
          body: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              //Spacer(),
              Container(
                padding: EdgeInsets.only(top: 20),
                child: Text(
                  'Bienvenido',
                  style: TextStyle(
                      color: Colors.black, fontFamily: 'Roboto', fontSize: 15),
                ),
              ),

              Container(
                  padding: EdgeInsets.all(20),
                  margin: EdgeInsets.all(30),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.black.withOpacity(0.05)),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                          child: Container(
                        //color: Colors.red,
                        height: 50,
                        //padding: EdgeInsets.all(0),
                        //margin: EdgeInsets.all(10),
                        child: Column(children: <Widget>[
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 2,
                                groupValue: radioValue1,
                                activeColor: Colors.red.shade600,
                                onChanged: (val) {
                                  print("$val");
                                  selected(val);
                                  setVisible = true;
                                },
                              ),
                              Text(
                                "Crear cuenta",
                                style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "¿Eres nuevo?",
                                style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 15,
                                ),
                              ),
                            ],
                          ),
                        ]),
                      )),
                      Form(
                        key: _keyForm,
                        child: Visibility(
                            visible: setVisible,
                            child: Column(children: <Widget>[
                              Visibility(
                                visible: change,
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  width: double.infinity,
                                  child: Text(
                                    'Ocurrio un problema. Completa todos los campos',
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontSize: 13,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                              DataFill(
                                text: 'Escribe tu nombre',
                                parameter: _name,
                                value: false,
                              ),
                              DataFill(
                                text: 'Ingrese su correo',
                                parameter: _email,
                                value: false,
                              ),
                              Visibility(
                                visible: verEm,
                                child: Container(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    width: double.infinity,
                                    child: Text(
                                        'El correo electrónico que ingresó no es válido o ya ha sido usado por otro usuario. Intente con otro',
                                        textAlign: TextAlign.justify,
                                        style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 12,
                                          fontStyle: FontStyle.italic,
                                          color: Colors.red.shade800,
                                        ))),
                              ),
                              DataFill(
                                text: 'Cree una contraseña',
                                parameter: _password,
                                value: _val,
                              ),
                              Visibility(
                                visible: verPass,
                                child: Container(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    width: double.infinity,
                                    child: Text(
                                      'La contraseña debe tener por lo menos 6 caracteres',
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: 12,
                                        fontStyle: FontStyle.italic,
                                        color: Colors.red.shade800,
                                      ),
                                    )),
                              ),
                              Row(
                                children: <Widget>[
                                  Checkbox(
                                      value: checkValue,
                                      onChanged: (val) {
                                        selectedCheck(val);
                                        _val = !_val;
                                      }),
                                  Text(
                                    'Mostrar contraseña',
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                              Row(children: <Widget>[
                                Radio(
                                  value: 1,
                                  groupValue: radioValue11,
                                  activeColor: Colors.blue.shade600,
                                  onChanged: (val) {
                                    selected1(val);
                                    form = false;
                                    usuario = true;
                                  },
                                ),
                                Text(
                                  'Registrarse como USUARIO',
                                  style: TextStyle(
                                    fontFamily: 'Roboto',
                                    fontSize: 12,
                                  ),
                                )
                              ]),
                              Row(
                                children: <Widget>[
                                  Radio(
                                    value: 2,
                                    groupValue: radioValue11,
                                    activeColor: Colors.blue.shade600,
                                    onChanged: (val) {
                                      selected1(val);
                                      form = true;
                                      usuario = false;
                                    },
                                  ),
                                  Text(
                                    'Registrarse como ARTESANO',
                                    style: TextStyle(
                                      fontFamily: 'Roboto',
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                              Visibility(
                                  visible: form,
                                  child: ModalProgressHUD(
                                      inAsyncCall: _isInAsyncCall,
                                      opacity: 0.5,
                                      dismissible: false,
                                      progressIndicator:
                                          CircularProgressIndicator(),
                                      color: Colors.blueGrey,
                                      child: SingleChildScrollView(
                                          padding: EdgeInsets.only(
                                              left: 10, right: 15),
                                          child: Form(
                                            key: _formKey,
                                            child: Column(children: <Widget>[
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(top: 10),
                                              ),
                                              Container(
                                                padding: EdgeInsets.all(15),
                                                decoration: BoxDecoration(
                                                    color: Colors.grey.shade300,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                margin: EdgeInsets.only(
                                                    top: 15,
                                                    left: 15,
                                                    right: 15,
                                                    bottom: 15),
                                                child: Column(
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Text(
                                                      'FORMULARIO DE REGISTRO PARA ARTESANOS DE BOLIVIA',
                                                      style: TextStyle(
                                                        fontFamily: 'Roboto',
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Text(
                                                      'Este formulario es epsecíficamente para registrar a ARTESANOS BOLIVIANOS que deseen vender sus productos a traves de esta plataforma. De no ser uno, absténgase de llenarlo. Los datos serán corroborados',
                                                      style: TextStyle(
                                                        color: Colors
                                                            .blue.shade900,
                                                        fontFamily: 'Roboto',
                                                        fontSize: 12,
                                                        //fontWeight: FontWeight.bold,
                                                        fontStyle:
                                                            FontStyle.italic,
                                                      ),
                                                      textAlign:
                                                          TextAlign.justify,
                                                    ),
                                                    Text(
                                                      'Nombres',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 12),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.8)),
                                                      height: 25,
                                                      child: TextFormField(
                                                        controller: _nameArt,
                                                        textAlign:
                                                            TextAlign.center,
                                                        autofocus: true,
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 15,
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.4),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize: 12),
                                                          hintText:
                                                              ' p.e. Julio Augusto',
                                                        ),
                                                        validator:
                                                            (String val) {
                                                          if (val.isEmpty) {
                                                            return 'Escriba algun texto';
                                                          }
                                                          return null;
                                                        },
                                                        onSaved: (value) =>
                                                            name = value.trim(),
                                                      ),
                                                    ),
                                                    Text(
                                                      'Apellidos',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 12),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.8)),
                                                      height: 25,
                                                      child: TextFormField(
                                                        controller: _apellido,
                                                        textAlign:
                                                            TextAlign.center,
                                                        autofocus: true,
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 15,
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.4),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize: 12),
                                                          hintText:
                                                              ' p.e. Quispe Flores',
                                                        ),
                                                        validator:
                                                            (String val) {
                                                          if (val.isEmpty) {
                                                            return 'Escriba algun texto';
                                                          }
                                                          return null;
                                                        },
                                                        onSaved: (value) =>
                                                            lastName =
                                                                value.trim(),
                                                      ),
                                                    ),
                                                    Text(
                                                      'Dirección actual (calle, #, zona u otro)',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 12),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.8)),
                                                      height: 25,
                                                      child: TextFormField(
                                                        controller: _direccion,
                                                        textAlign:
                                                            TextAlign.center,
                                                        autofocus: true,
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 15,
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.4),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize: 12),
                                                          hintText:
                                                              ' p.e. Calle 6 de Agosto #10025, Zona Los Olivos',
                                                        ),
                                                        validator:
                                                            (String val) {
                                                          if (val.isEmpty) {
                                                            return 'Escriba algun texto';
                                                          }
                                                          return null;
                                                        },
                                                        onSaved: (value) =>
                                                            direccion =
                                                                value.trim(),
                                                      ),
                                                    ),
                                                    Text(
                                                      'Numero de referencia para contactarnos contigo',
                                                      style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 12),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.8)),
                                                      height: 25,
                                                      child: TextFormField(
                                                        controller: _telefono,
                                                        textAlign:
                                                            TextAlign.center,
                                                        autofocus: true,
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 15,
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.4),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize: 12),
                                                          hintText:
                                                              ' p.e. 65891278',
                                                        ),
                                                        validator:
                                                            (String val) {
                                                          if (val.isEmpty) {
                                                            return 'Escriba algun texto';
                                                          }
                                                          return null;
                                                        },
                                                        onSaved: (value) =>
                                                            cell = int.parse(
                                                                value),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: double.infinity,
                                                      child: Text(
                                                        '¿Pertenece a alguna asociación de artesanos?',
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    Row(children: <Widget>[
                                                      Radio(
                                                        value: 1,
                                                        groupValue: radioValue2,
                                                        activeColor: Colors
                                                            .green.shade600,
                                                        onChanged: (val) {
                                                          selected2(val);
                                                          pertenece = true;
                                                        },
                                                      ),
                                                      Text(
                                                        'SI',
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 12,
                                                        ),
                                                      )
                                                    ]),
                                                    Row(
                                                      children: <Widget>[
                                                        Radio(
                                                          value: 2,
                                                          groupValue:
                                                              radioValue2,
                                                          activeColor: Colors
                                                              .green.shade600,
                                                          onChanged: (val) {
                                                            selected2(val);
                                                            pertenece = false;
                                                          },
                                                        ),
                                                        Text(
                                                          'NO',
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Roboto',
                                                            fontSize: 12,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Visibility(
                                                        visible: pertenece,
                                                        child: Column(
                                                          children: <Widget>[
                                                            Container(
                                                              width: double
                                                                  .infinity,
                                                              child: Text(
                                                                'Nombre de la Asociación de artesanos de tu comunidad',
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12),
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .all(5),
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  color: Colors
                                                                      .white
                                                                      .withOpacity(
                                                                          0.8)),
                                                              height: 25,
                                                              child:
                                                                  TextFormField(
                                                                controller:
                                                                    _asociacion,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                autofocus: true,
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Roboto',
                                                                  fontSize: 15,
                                                                ),
                                                                decoration:
                                                                    InputDecoration(
                                                                  border:
                                                                      InputBorder
                                                                          .none,
                                                                  hintStyle: TextStyle(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              0.4),
                                                                      fontFamily:
                                                                          'Roboto',
                                                                      fontSize:
                                                                          12),
                                                                  hintText:
                                                                      ' p.e. Asociacion de artesanos de Pacajes',
                                                                ),
                                                                validator:
                                                                    (String
                                                                        val) {
                                                                  if (val
                                                                      .isEmpty) {
                                                                    return 'Escriba algun texto';
                                                                  }
                                                                  return null;
                                                                },
                                                                onSaved: (value) =>
                                                                    asociacion =
                                                                        value
                                                                            .trim(),
                                                              ),
                                                            ),
                                                            Container(
                                                              width: double
                                                                  .infinity,
                                                              child: Text(
                                                                'Nombre de tu comunidad, provincia u otro',
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12),
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .all(5),
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  color: Colors
                                                                      .white
                                                                      .withOpacity(
                                                                          0.8)),
                                                              height: 25,
                                                              child:
                                                                  TextFormField(
                                                                controller:
                                                                    _comunidad,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                autofocus: true,
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Roboto',
                                                                  fontSize: 15,
                                                                ),
                                                                decoration:
                                                                    InputDecoration(
                                                                  border:
                                                                      InputBorder
                                                                          .none,
                                                                  hintStyle: TextStyle(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              0.4),
                                                                      fontFamily:
                                                                          'Roboto',
                                                                      fontSize:
                                                                          12),
                                                                  hintText:
                                                                      ' p.e. Provincia Pacajes',
                                                                ),
                                                                validator:
                                                                    (String
                                                                        val) {
                                                                  if (val
                                                                      .isEmpty) {
                                                                    return 'Escriba algun texto';
                                                                  }
                                                                  return null;
                                                                },
                                                                onSaved: (value) =>
                                                                    comunidad =
                                                                        value
                                                                            .trim(),
                                                              ),
                                                            ),
                                                          ],
                                                        )),
                                                    Container(
                                                      width: double.infinity,
                                                      child: Text(
                                                        '¿Que materiales utilizas para realizar tus artesanías?',
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.8)),
                                                      height: 25,
                                                      child: TextFormField(
                                                        controller: _material,
                                                        textAlign:
                                                            TextAlign.center,
                                                        autofocus: true,
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 15,
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.4),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize: 12),
                                                          hintText:
                                                              ' p.e. cuero, aguayo y tela popelina',
                                                        ),
                                                        validator:
                                                            (String val) {
                                                          if (val.isEmpty) {
                                                            return 'Escriba algun texto';
                                                          }
                                                          return null;
                                                        },
                                                        onSaved: (value) =>
                                                            materiales =
                                                                value.trim(),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: double.infinity,
                                                      child: Text(
                                                        'Danos una breve descripcion de los tipos de Artesanias que realizas',
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          color: Colors.white
                                                              .withOpacity(
                                                                  0.8)),
                                                      height: 25,
                                                      child: TextFormField(
                                                        controller:
                                                            _descripcion,
                                                        textAlign:
                                                            TextAlign.center,
                                                        autofocus: true,
                                                        style: TextStyle(
                                                          fontFamily: 'Roboto',
                                                          fontSize: 15,
                                                        ),
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              InputBorder.none,
                                                          hintStyle: TextStyle(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.4),
                                                              fontFamily:
                                                                  'Roboto',
                                                              fontSize: 12),
                                                          hintText:
                                                              ' p.e. Realizo mochilas y carteras para dama',
                                                        ),
                                                        validator:
                                                            (String val) {
                                                          if (val.isEmpty) {
                                                            return 'Escriba algun texto';
                                                          }
                                                          return null;
                                                        },
                                                        onSaved: (value) =>
                                                            descripcion =
                                                                value.trim(),
                                                      ),
                                                    ),
                                                    /*Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              RaisedButton(
                                onPressed: _enviar,
                                child: Text('Crear Producto',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Roboto',
                                        fontSize: 15)),
                                color: Colors.green,
                              ),
                            ],
                          )*/
                                                  ],
                                                ),
                                              ),
                                            ]),
                                          )))),
                              Container(
                                  height: 30,
                                  padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(2)),
                                  child: FlatButton(
                                    onPressed: () async {
                                      if (usuario) {
                                        if (_keyForm.currentState.validate()) {
                                          if (await user.register(_email.text,
                                              _password.text, _name.text)) {
                                            if (!user.emailVerified &&
                                                _email.text
                                                    .toString()
                                                    .isValidEmail() &&
                                                !user.passVerified) {
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext
                                                          context) =>
                                                      FancyDialog(
                                                        title:
                                                            "Confirma tu Registro",
                                                        descreption:
                                                            "Verifique su cuenta en la bandeja de sucorreo",
                                                        animationType:
                                                            FancyAnimation
                                                                .BOTTOM_TOP,
                                                        theme: FancyTheme.FANCY,
                                                        gifPath:
                                                            './assets/images/buy2.gif',
                                                        okFun: () => {
                                                          Navigator.of(context).pop(
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (BuildContext
                                                                          context) {
                                                            return Register(
                                                                text: 'null');
                                                          }))
                                                        },
                                                      ));
                                            }
                                          }
                                        }
                                      } else {
                                        if (_keyForm.currentState.validate()) {
                                          if (await user.register(_email.text,
                                              _password.text, _name.text)) {
                                            if (!user.emailVerified &&
                                                _email.text
                                                    .toString()
                                                    .isValidEmail() &&
                                                !user.passVerified) {
                                              _enviar();
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext
                                                          context) =>
                                                      FancyDialog(
                                                        title: "Aceptar compra",
                                                        descreption:
                                                            "Verifique su cuenta en la bandeja de sucorreo",
                                                        animationType:
                                                            FancyAnimation
                                                                .BOTTOM_TOP,
                                                        theme: FancyTheme.FANCY,
                                                        gifPath:
                                                            './assets/images/buy2.gif',
                                                        okFun: () => {
                                                          Navigator.of(context).pop(
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (BuildContext
                                                                          context) {
                                                            return Register(
                                                                text: 'null');
                                                          }))
                                                        },
                                                      ));
                                            }
                                          }
                                        }

                                        //  print(_email.text.toString());
                                        setState(() {
                                          print(_email.text.toString());
                                          //change = true;
                                          if (_name.text.toString() == '' ||
                                              _password.text.toString() == '' ||
                                              _email.text.toString() == '') {
                                            change = true;
                                          } else {
                                            change = false;
                                          }
                                          if (user.emailVerified ||
                                              !_email.text
                                                  .toString()
                                                  .isValidEmail()) {
                                            verEm = true;
                                            print(_email.text);
                                          } else {
                                            verEm = false;
                                          }
                                          if (user.passVerified) {
                                            verPass = true;
                                          } else {
                                            verPass = false;
                                          }
                                        });
                                      }
                                    },
                                    child: Text(
                                      'Verificar',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Roboto',
                                          fontSize: 15),
                                    ),
                                  )),
                              Container(height: 15),
                              Text(
                                'Al crear una cuenta, aceptas las Condiciones de privacidad de Usoy el Aviso de Privacidad de CRAFTSBOL.com',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Roboto',
                                    fontSize: 10),
                              ),
                              Container(height: 15),
                              Text(
                                '¿Necesitas Ayuda?',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                              ),
                              GestureDetector(
                                  child: Container(
                                margin: EdgeInsets.only(top: 10),
                                //color: Colors.red,
                                height: 50,
                                //padding: EdgeInsets.all(0),
                                //margin: EdgeInsets.all(10),
                                child: Column(children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Radio(
                                        value: 1,
                                        groupValue: radioValue1,
                                        activeColor: Colors.red.shade600,
                                        onChanged: (val) {
                                          selected(val);
                                          setVisible = false;
                                        },
                                      ),
                                      Text(
                                        "Iniciar Sesion ",
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        "¿Ya eres cliente?",
                                        style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 15,
                                        ),
                                      ),
                                    ],
                                  ),
                                ]),
                              )),
                            ]),
                            replacement: Form(
                                key: _keyForm2,
                                child: Column(children: <Widget>[
                                  GestureDetector(
                                      child: Container(
                                    //color: Colors.red,
                                    height: 50,
                                    //padding: EdgeInsets.all(0),
                                    //margin: EdgeInsets.all(10),
                                    child: Column(children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Radio(
                                            value: 1,
                                            groupValue: radioValue1,
                                            activeColor: Colors.red.shade600,
                                            onChanged: (val) {
                                              print("$val");
                                              selected(val);
                                              setVisible = false;
                                            },
                                          ),
                                          Text(
                                            "Iniciar Sesion",
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            " ¿Ya eres cliente?",
                                            style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: 15,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ]),
                                  )),
                                  DataFill(
                                    text: 'Ingresa tu correo',
                                    parameter: _emailSign,
                                    value: false,
                                  ),
                                  DataFill(
                                    text: 'Password',
                                    parameter: _passwordSign,
                                    value: _val2,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Checkbox(
                                          value: checkValue,
                                          onChanged: (val) {
                                            selectedCheck(val);
                                            _val2 = !_val2;
                                          }),
                                      Text(
                                        'Mostrar contraseña',
                                        style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(children: <Widget>[
                                    Row(children: <Widget>[
                                      Radio(
                                        value: 1,
                                        groupValue: radioValue111,
                                        activeColor: Colors.blue.shade600,
                                        onChanged: (val) {
                                          selected11(val);
                                          us = true;
                                          art = false;
                                        },
                                      ),
                                      Text(
                                        'Ingresar como USUARIO',
                                        style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 12,
                                        ),
                                      )
                                    ]),
                                    Row(
                                      children: <Widget>[
                                        Radio(
                                          value: 2,
                                          groupValue: radioValue111,
                                          activeColor: Colors.blue.shade600,
                                          onChanged: (val) {
                                            selected11(val);
                                            us = false;
                                            art = true;
                                          },
                                        ),
                                        Text(
                                          'Ingresar como ARTESANO',
                                          style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    )
                                  ]),
                                  Container(
                                      margin: EdgeInsets.only(top: 10),
                                      height: 30,
                                      padding: EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          borderRadius:
                                              BorderRadius.circular(2)),
                                      child: FlatButton(
                                        onPressed: () async {
                                          if (_keyForm2.currentState
                                              .validate()) {
                                            if (!await user.signIn(
                                                _emailSign.text,
                                                _passwordSign.text)) {
                                              if (art) {
                                                for (var item in listaArt) {
                                                  email.add(item.id.toString());
                                                }
                                                if (!email.contains(_emailSign
                                                    .text
                                                    .toString())) {
                                                  createAlertDialog2(context);
                                                } else {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) {
                                                    return HomePage(
                                                      text:
                                                          'user.email.toString()',
                                                    );
                                                  }));
                                                }
                                              }

                                              if (us) {
                                                if (user.notvalue) {
                                                  createAlertDialog(context);
                                                } else {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (context) {
                                                    return HomePage(
                                                      text:
                                                          'user.email.toString()',
                                                    );
                                                  }));
                                                }
                                              }
                                            }
                                          }
                                        },
                                        /*async {
                                      if (_keyForm2.currentState.validate()) {
                                        _signIn();
                                      }
                                    },*/
                                        child: Text(
                                          'Continuar',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Roboto',
                                              fontSize: 15),
                                        ),
                                      )),
                                  Container(height: 15),
                                  Text(
                                    'Al crear una cuenta, aceptas las Condiciones de privacidad de Usoy el Aviso de Privacidad de CRAFTSBOL.com',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Roboto',
                                        fontSize: 10),
                                  ),
                                  Container(height: 15),
                                  Text(
                                    '¿Necesitas Ayuda?',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'Roboto',
                                        fontSize: 12),
                                  ),
                                ]))),
                      )
                    ],
                  )),
            ],
          )),
        ));
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
/*void _register() async {
    final User user = (await _auth.createUserWithEmailAndPassword(
            email: _email.text, password: _password.text))
        .user;
    if (user != null) {
      if (!user.emailVerified) {
        await user.sendEmailVerification();
      }
      await user.updateProfile(displayName: _name.text);
      //final user1 = _auth.currentUser;
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Register(
                text: 'Reg',
              )));
    }
  }*/

/* void _signIn() async {
    try {
      final User user = (await _auth2.signInWithEmailAndPassword(
              email: _emailSign.text, password: _passwordSign.text))
          .user;
      if (!user.emailVerified) {
        await user.sendEmailVerification();
      }
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        return HomePage(
          text: 'user.email.toString()',
          user: user,
          init: false,
        );
      }));
    } catch (e) {
      print('hello');
    }
  }*/

class DataFill extends StatelessWidget {
  final bool value;
  final String text;
  final TextEditingController parameter;
  const DataFill({Key key, this.text, this.parameter, this.value})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.only(),
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white.withOpacity(0.8)),
      height: 25,
      child: TextFormField(
        autovalidateMode: AutovalidateMode.always,
        obscureText: value,
        controller: parameter,
        textAlign: TextAlign.center,
        autofocus: true,
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 15,
        ),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintStyle: TextStyle(
              color: Colors.black.withOpacity(0.4),
              fontFamily: 'Roboto',
              fontSize: 12),
          hintText: text,
        ),
        //validator:
        /*(String val) {
          if (val.isEmpty) {
            return 'done';
          }
          return null;
        },*/
      ),
    );
  }
}

class DataFill2 extends StatelessWidget {
  final bool value;
  final String text;
  final TextEditingController parameter;
  const DataFill2({Key key, this.text, this.parameter, this.value})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.only(),
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white.withOpacity(0.8)),
      height: 25,
      child: TextFormField(
        obscureText: value,
        controller: parameter,
        textAlign: TextAlign.center,
        autofocus: true,
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 15,
        ),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintStyle: TextStyle(
              color: Colors.black.withOpacity(0.4),
              fontFamily: 'Roboto',
              fontSize: 12),
          hintText: text,
        ),
        validator: (String val) {
          if (val.isEmpty) {
            return 'Escriba algun texto';
          }
          return null;
        },
      ),
    );
  }
}
