import 'package:flutter/material.dart';

class Help extends StatelessWidget {
  static const routeHelp = 'help';
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        "help",
        style: TextStyle(
            fontFamily: 'Sans', fontSize: 10.0, fontWeight: FontWeight.bold),
      ),
    );
  }
}
