import 'package:flutter/material.dart';

class ProdCatalog extends StatelessWidget {
  static const routeProd = 'products';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "PRODUCTOS",
          style: TextStyle(
            fontFamily: 'Sans',
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        child: new Image(
          image: AssetImage('assets/images/logo2.png'),
          width: 200,
          // ...
        ),
        // ...
      ),
    );
  }
}
