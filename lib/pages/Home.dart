//import 'package:artbol/pages/ShopCar.dart';
import 'dart:async';
import 'dart:ui';

import 'package:artbol/pagesSubmenu/firebaseService.dart';
import 'package:artbol/pagesSubmenu/pedido.dart';
import 'package:artbol/pagesSubmenu/prod_model.dart';
//import 'package:artbol/pagesSubmenu/update.dart';
//import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:artbol/side_bar/side_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  final User user;
  final bool init;
  static const routeHome = 'routeHome';
  final String text;
  const HomePage({Key key, @required this.text, this.user, this.init})
      : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // List<ProdModel> _listaCar = [];
  bool isDropDown = false;
  OverlayEntry floatingBar;
  //final FirebaseAuth _auth = FirebaseAuth.instance;
  GlobalKey actionKey;
  String hover = 'Que deseas buscar';
  TextEditingController controlador = TextEditingController();
  final searches = [
    'cocina',
    'artesanias de jardin',
    'mochila',
    'artesanias de pared',
    'zapatos',
    'joyas',
    'qui'
  ];

  final recent = ['mochila', 'artesanias de pared', 'zapatos', 'joyas'];
  bool start;
  List<ProdModel> listaProd = List<ProdModel>();
  //List<ProdModel> _listaCar = [];
  FirebaseService db = new FirebaseService();
  StreamSubscription<QuerySnapshot> productSub;

  bool st1 = false, st2 = false, st3 = false, st4 = false, st5 = false;
  @override
  void initState() {
    actionKey = LabeledGlobalKey(widget.text);
    super.initState();
    listaProd = new List();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
  }

  OverlayEntry _createFloatingBar() {
    final query = [hover];
    final sugestSearch = query.isEmpty
        ? recent
        : searches.where((word) => word.startsWith(query.toString())).toList();
    return OverlayEntry(builder: (context) {
      return Container(
        child: Positioned(
            width: MediaQuery.of(context).size.width,
            top: 100,
            height: 20,
            child: Container(
                child: ListView.builder(
              itemBuilder: (context, index) => ListTile(
                title: Text(sugestSearch[index],
                    style: TextStyle(fontFamily: 'Roboto', fontSize: 15)),
                trailing: Icon(Icons.arrow_forward),
              ),
              itemCount: sugestSearch.length,
            ))),
      );
    });
  }

  List<String> images = [
    'assets/images/UP1.jpg',
    'assets/images/UP2.jpg',
    'assets/images/UP3.jpg',
    'assets/images/UP4.jpg',
    'assets/images/UP5.jpg',
    'assets/images/UP6.jpg',
    'assets/images/UP7.jpg',
    'assets/images/UP8.jpg',
    'assets/images/UP9.jpg',
    'assets/images/UP10.jpg',
    'assets/images/UP11.jpg'
  ];
  int photoIndex = 0;
  void previousImage() {
    setState(() {
      photoIndex = photoIndex > 0 ? photoIndex - 1 : 0;
    });
  }

  void nextImage() {
    setState(() {
      photoIndex = photoIndex < images.length - 1 ? photoIndex + 1 : photoIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = (MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height));
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: Scaffold(
            drawer: SideBar(),
            appBar: PreferredSize(
                preferredSize:
                    Size.fromHeight(100.0), // here the desired height
                child: AppBar(
                  // hides leading widget
                  flexibleSpace: Column(children: <Widget>[
                    Container(
                      height: 30,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          width: 50,
                        ),
                        new Image(
                            height: 50,
                            width: MediaQuery.of(context).size.width / 3,
                            image: AssetImage('assets/images/logo2.png'),
                            alignment: Alignment.centerRight),
                        Spacer(),
                        /*Padding(
                          padding: EdgeInsets.only(right: 15), child: ShopCar(_listaCar)
                          /* IconButton(
                            icon: Icon(Icons.add_shopping_cart,
                                color: Colors.white), 
                            onPressed: () {  },),*/
                          ),*/
                        /*Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: ShopCar(user.listaCar)),*/
                        Stack(children: <Widget>[
                          IconButton(
                            icon: Icon(
                              Icons.shopping_cart,
                              color: Colors.white,
                              size: 25,
                            ),
                            onPressed: () {
                              for (var i = 0; i < user.listaCar.length; i++) {
                                print(user.listaCar[i].toMap().toString());
                              }
                              if (user.notvalue) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) {
                                  return Cart(user.listaCar);
                                  //Cart(lista);
                                }));
                              } else {
                                if (user.listaCar.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (BuildContext context) {
                                    return Cart(user.listaCar);
                                  }));
                                }
                              }
                            },
                          ),
                          if (user.listaCar.length > 0)
                            Padding(
                                padding: EdgeInsets.only(left: 17, top: 5),
                                child: CircleAvatar(
                                  radius: 8,
                                  backgroundColor: Colors.blue,
                                  foregroundColor: Colors.white,
                                  child: Text(
                                    user.listaCar.length.toString(),
                                    style: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ))
                        ])
                      ],
                    ),
                    Container(
                        height: 25,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white.withOpacity(0.16),
                        ),
                        margin: EdgeInsets.all(5),
                        width: double.infinity,
                        //color: Colors.black.withOpacity(0.2),
                        child: Row(children: <Widget>[
                          IconButton(
                            padding: EdgeInsets.all(0),
                            iconSize: 20,
                            icon: Icon(
                              Icons.search,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              /*setState(() {
                          SearchCont();
                        });*/

                              //showSearch(context: context, delegate: Search());
                            },
                          ),
                          Flexible(
                              child: TextFormField(
                            controller: controlador,
                            key: actionKey,
                            style: TextStyle(color: Colors.white),
                            cursorColor: Colors.white,
                            decoration: InputDecoration(
                                hintText: hover,
                                hintStyle: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 12,
                                  color: Colors.white.withOpacity(0.30),
                                )),
                            onTap: () {
                              setState(() {
                                if (!isDropDown) {
                                  //Color.fromARGB(255, 213, 225, 221);
                                  Color.fromARGB(255, 213, 225, 221);
                                  floatingBar = _createFloatingBar();
                                  Overlay.of(context).insert(floatingBar);
                                }

                                isDropDown = !isDropDown;
                              });
                            },
                            // controller: controlador,
                            //onChanged: (texto) {
                            //controlador.text = texto;
                            //}
                            autofocus: true,
                          )),
                          IconButton(
                            padding: EdgeInsets.all(0),
                            iconSize: 20,
                            icon: Icon(
                              Icons.close,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                //SearchCont();
                                controlador.text = '';
                              });

                              //showSearch(context: context, delegate: Search());
                            },
                          ),
                          /*Text(hover,
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.25),
                            fontFamily: 'Roboto',
                            fontSize: 15))*/
                        ])),
                  ]),
                )),
            body: SafeArea(
              child: SingleChildScrollView(
                  child: Container(
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        //WaveClip(),
                        Container(
                          color: Colors.red.shade900,
                          padding: const EdgeInsets.only(
                              left: 10, top: 20, bottom: 10),
                          height: 180,
                          child: ListView.builder(
                            itemCount: images.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return Row(
                                children: <Widget>[
                                  Container(
                                    height: 300,
                                    padding: new EdgeInsets.only(
                                        left: 10.0, bottom: 10.0),
                                    child: Card(
                                      elevation: 9.0,
                                      clipBehavior: Clip.antiAlias,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(24)),
                                      child: AspectRatio(
                                        aspectRatio: 1,
                                        child: Image(
                                          image: AssetImage(images[index]),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    Container(height: 3.0, color: Colors.grey),
                    SizedBox(
                      height: 5.0,
                    ),
                    Container(
                      color: Colors.grey[300],
                      height: MediaQuery.of(context).size.height / 1.5,
                      child: GridView.builder(
                        shrinkWrap: true,
                        padding: EdgeInsets.all(5),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount:
                                (orientation == Orientation.portrait) ? 2 : 3,
                            childAspectRatio:
                                (orientation == Orientation.portrait)
                                    ? size2 + 0.15
                                    : size1 + 0.15),
                        itemCount: listaProd.length,
                        itemBuilder: (context, index) {
                          var item = listaProd[index];

                          return Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.all(5),
                            elevation: 4,
                            color: Colors.grey.shade300,
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.all(5),
                                      child: CachedNetworkImage(
                                          imageUrl:
                                              '${listaProd[index].image}' +
                                                  '?alt=media',
                                          fit: BoxFit.contain,
                                          placeholder: (_, __) {
                                            return Center(
                                                child:
                                                    CupertinoActivityIndicator(
                                              radius: 15,
                                            ));
                                          })),
                                ),
                                Row(children: <Widget>[
                                  GestureDetector(
                                    child: (!st1)
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, left: 5, bottom: 10),
                                            child: Icon(
                                              Icons.star_border,
                                              color: Colors.black,
                                            ),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, left: 5, bottom: 10),
                                            child: Icon(Icons.star,
                                                color: Colors.green.shade800),
                                          ),
                                    onTap: () {
                                      setState(() {
                                        st1 = true;
                                        st2 = false;
                                        st3 = false;
                                        st4 = false;
                                        st5 = false;
                                      });
                                    },
                                  ),
                                  GestureDetector(
                                    child: (!st2)
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(
                                              Icons.star_border,
                                              color: Colors.black,
                                            ),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(Icons.star,
                                                color: Colors.green.shade800),
                                          ),
                                    onTap: () {
                                      setState(() {
                                        st1 = true;
                                        st2 = true;
                                        st3 = false;
                                        st4 = false;
                                        st5 = false;
                                      });
                                    },
                                  ),
                                  GestureDetector(
                                    child: (!st3)
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(
                                              Icons.star_border,
                                              color: Colors.black,
                                            ),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(Icons.star,
                                                color: Colors.green.shade800),
                                          ),
                                    onTap: () {
                                      setState(() {
                                        st1 = true;
                                        st2 = true;
                                        st3 = true;
                                        st4 = false;
                                        st5 = false;
                                      });
                                    },
                                  ),
                                  GestureDetector(
                                    child: (!st4)
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(
                                              Icons.star_border,
                                              color: Colors.black,
                                            ),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(Icons.star,
                                                color: Colors.green.shade800),
                                          ),
                                    onTap: () {
                                      setState(() {
                                        st1 = true;
                                        st2 = true;
                                        st3 = true;
                                        st4 = true;
                                        st5 = false;
                                      });
                                    },
                                  ),
                                  GestureDetector(
                                    child: (!st5)
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(
                                              Icons.star_border,
                                              color: Colors.black,
                                            ),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Icon(Icons.star,
                                                color: Colors.green.shade800),
                                          ),
                                    onTap: () {
                                      setState(() {
                                        st1 = true;
                                        st2 = true;
                                        st3 = true;
                                        st4 = true;
                                        st5 = true;
                                      });
                                    },
                                  ),
                                ]),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 5, right: 5, bottom: 2),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Container(
                                          child: Flexible(
                                        fit: FlexFit.tight,
                                        child: Text(
                                          '${listaProd[index].name}',
                                          maxLines: 2,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ))
                                    ],
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(left: 5, right: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Container(
                                            child: Flexible(
                                          fit: FlexFit.tight,
                                          child: Text(
                                            '${listaProd[index].descripcion}',
                                            maxLines: 3,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                              fontFamily: 'Roboto',
                                              fontSize: 10,
                                            ),
                                          ),
                                        ))
                                      ],
                                    )),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Flexible(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            top: 5,
                                            left: 5,
                                            bottom: 10,
                                            right: 0),
                                        child: Text(
                                            '${listaProd[index].price}'
                                                    .toString() +
                                                ' Bs',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Roboto',
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ),
                                    /*Visibility(
                                  visible: user.value,
                                  child: GestureDetector(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 10, right: 0, bottom: 10),
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.red.shade800,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        deleteData(index);
                                      });
                                    },
                                  ),
                                ),
                                Visibility(
                                  visible: user.value,
                                  child: GestureDetector(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 10, right: 0, bottom: 10),
                                      child: Icon(
                                        Icons.edit,
                                        color: Colors.blue.shade800,
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) {
                                        return Update(index, 'productos');
                                      }));
                                    },
                                  ),
                                ),*/
                                    GestureDetector(
                                      child: (!user.images.contains(item))
                                          ? Padding(
                                              padding: EdgeInsets.only(
                                                  top: 10,
                                                  right: 5,
                                                  bottom: 10),
                                              child: Icon(
                                                Icons.add_shopping_cart,
                                                color: Colors.black,
                                              ),
                                            )
                                          : Padding(
                                              padding: EdgeInsets.only(
                                                  top: 10,
                                                  right: 5,
                                                  bottom: 10),
                                              child: Icon(
                                                  Icons.add_shopping_cart,
                                                  color: Colors.green),
                                            ),
                                      onTap: () {
                                        setState(() {
                                          if (!user.listaCar.contains(item)) {
                                            print(user.listaCar.length
                                                .toString());
                                            //_listaCar.add(item);
                                            user.listaCar.add(item);
                                            //user.images.add(item.name);
                                          } else {
                                            print('remove');
                                            print(user.listaCar.length
                                                .toString());
                                            //_listaCar.remove(item);
                                            user.listaCar.remove(item);
                                          }
                                          print(user.value);
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      ),

                      /*GridView.builder(
                      padding: const EdgeInsets.all(4.0),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                      itemCount: _productosModel.length,
                      itemBuilder: (context, index) {
                        final String imagen = _productosModel[index].image;
                        var item = _productosModel[index];
                        return Card(
                            elevation: 4.0,
                            child: Stack(
                              fit: StackFit.loose,
                              alignment: Alignment.center,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: CachedNetworkImage(
                                          imageUrl:
                                              '${_productosModel[index].image}' +
                                                  '?alt=media',
                                          fit: BoxFit.cover,
                                          placeholder: (_, __) {
                                            return Center(
                                                child:
                                                    CupertinoActivityIndicator(
                                              radius: 15,
                                            ));
                                          }),
                                    ),
                                    Text(
                                      '${_productosModel[index].name}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 25,
                                        ),
                                        Text(
                                          '${_productosModel[index].price.toString()}',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 23.0,
                                              color: Colors.black),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            right: 8.0,
                                            bottom: 8.0,
                                          ),
                                          child: Align(
                                            alignment: Alignment.bottomRight,
                                            child: GestureDetector(
                                              child:
                                                  (!_listaCarro.contains(item))
                                                      ? Icon(
                                                          Icons.shopping_cart,
                                                          color: Colors.green,
                                                          size: 38,
                                                        )
                                                      : Icon(
                                                          Icons.shopping_cart,
                                                          color: Colors.red,
                                                          size: 38,
                                                        ),
                                              onTap: () {
                                                setState(() {
                                                  if (!_listaCarro
                                                      .contains(item))
                                                    _listaCarro.add(item);
                                                  else
                                                    _listaCarro.remove(item);
                                                });
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ));
                      },
                    )*/
                    ),
                  ],
                ),
              )),
            )));
  }
}
/*Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              /*Center(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            image: DecorationImage(
                                image: AssetImage(images[photoIndex]),
                                fit: BoxFit.cover)),
                        height: 200.0,
                        width: MediaQuery.of(context).size.width - 20,
                      ),
                      Positioned(
                        top: 375.0,
                        left: 25.0,
                        right: 25.0,
                        child: SelectedPhoto(
                            numberOfDots: images.length,
                            photoIndex: photoIndex),
                      )
                    ],
                  ),
                ),*/
              /* Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                      child: Text('Next'),
                      onPressed: nextImage,
                      elevation: 5.0,
                      color: Colors.green,
                    ),
                    SizedBox(width: 10.0),
                    RaisedButton(
                      child: Text('Prev'),
                      onPressed: previousImage,
                      elevation: 5.0,
                      color: Colors.blue,
                    )
                  ],
                ),*/
              //Row(children: <Widget>[

              /*/
            ],
          ),*/
          // ],
        ));
    //);
  }
}

class Search extends SearchDelegate<String> {
  final searches = [
    'cocina',
    'artesanias de jardin',
    'mochila',
    'artesanias de pared',
    'zapatos',
    'joyas'
  ];
  final recentSearches = ['mochila', 'artesanias de pared', 'zapatos', 'joyas'];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      iconSize: 20,
      icon: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final sugestSearch = query.isEmpty
        ? recentSearches
        : searches.where((word) => word.startsWith(query)).toList();
    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        title: Text(sugestSearch[index],
            style: TextStyle(fontFamily: 'Roboto', fontSize: 15)),
        trailing: Icon(Icons.arrow_forward),
      ),
      itemCount: sugestSearch.length,
    );
  }
}

class SearchCont extends StatefulWidget {
  @override
  _SearchContState createState() => _SearchContState();
}

class _SearchContState extends State<SearchCont> {
  final searches = [
    'cocina',
    'artesanias de jardin',
    'mochila',
    'artesanias de pared',
    'zapatos',
    'joyas'
  ];

  final recent = ['mochila', 'artesanias de pared', 'zapatos', 'joyas'];

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 100),
        //height: MediaQuery.of(context).size.height-100;
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.red,
        ),
        margin: EdgeInsets.all(5),
        width: double.infinity,
        //color: Colors.black.withOpacity(0.2),
        child: ListView.builder(
          itemBuilder: (context, index) => ListTile(
            title: Text(recent[index],
                style: TextStyle(fontFamily: 'Roboto', fontSize: 15)),
            trailing: Icon(Icons.arrow_forward),
          ),
        ));
  }
}*/

/*class SelectedPhoto extends StatelessWidget {
  final int numberOfDots;
  final int photoIndex;

  SelectedPhoto({this.numberOfDots, this.photoIndex});

  Widget _inactivePhoto() {
    return new Container(
        child: new Padding(
      padding: const EdgeInsets.only(left: 3.0, right: 3.0),
      child: Container(
        height: 8.0,
        width: 8.0,
        decoration: BoxDecoration(
            color: Colors.grey, borderRadius: BorderRadius.circular(4.0)),
      ),
    ));
  }

  Widget _activePhoto() {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(left: 3.0, right: 3.0),
        child: Container(
          height: 10.0,
          width: 10.0,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey, spreadRadius: 0.0, blurRadius: 2.0)
              ]),
        ),
      ),
    );
  }

  List<Widget> _buildDots() {
    List<Widget> dots = [];

    for (int i = 0; i < numberOfDots; ++i) {
      dots.add(i == photoIndex ? _activePhoto() : _inactivePhoto());
    }

    return dots;
  }

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildDots(),
      ),
    );
  }
*/
