//import 'package:artbol/pagesSubmenu/pedido.dart';
import 'package:artbol/pagesSubmenu/pedido.dart';
import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShopCar extends StatefulWidget {
  final List<ProdModel> _pedido;
  ShopCar(this._pedido);
  @override
  _ShopCarState createState() => _ShopCarState(this._pedido);
}

class _ShopCarState extends State<ShopCar> {
  _ShopCarState(this._pedido);
  List<ProdModel> _pedido;
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: Stack(
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
                size: 25,
              ),
              onPressed: () {
                if (user.notvalue) {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (BuildContext context) {
                    return Cart(_pedido);
                    //Cart(lista);
                  }));
                } else {
                  if (_pedido.isNotEmpty) {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Cart(_pedido);
                      //Cart(lista);
                    }));
                  }
                }
              },
            ),
            if (_pedido.length > 0)
              Padding(
                  padding: EdgeInsets.only(left: 17, top: 5),
                  child: CircleAvatar(
                    radius: 8,
                    backgroundColor: Colors.blue,
                    foregroundColor: Colors.white,
                    child: Text(
                      _pedido.length.toString(),
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 10,
                          fontWeight: FontWeight.bold),
                    ),
                  ))
          ],
        ));
  }
}
