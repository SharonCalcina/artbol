class ProdModel {
  var id;
  String name;
  String image;
  String descripcion;
  int price;
  int quantity;

  ProdModel(
    String docId,
    String name,
    String image,
    String descripcion,
    int price,
    int quantity,
  );
  ProdModel.map(dynamic obj) {
    this.id = obj['id'];
    this.name = obj['name'];
    this.image = obj['image'];
    this.descripcion = obj['desc'];
    this.price = obj['price'];
    this.quantity = obj['quantity'];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['image'] = image;
    map['desc'] = descripcion;
    map['price'] = price;
    map['quantity'] = quantity;
    return map;
  }

  ProdModel.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.name = map['name'];
    this.image = map['image'];
    this.descripcion = map['desc'];
    this.price = map['price'];
    this.quantity = 0;
  }
}
