import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'crear_prod.dart';

class Vender extends StatefulWidget {
  @override
  _VenderState createState() => _VenderState();
}

class _VenderState extends State<Vender> {
 
  bool _isInAsyncCall = false;
  int price;
  TextEditingController _nameArt = TextEditingController();
  TextEditingController _apellido = TextEditingController();
  TextEditingController _direccion = TextEditingController();
  TextEditingController _telefono = TextEditingController();
  TextEditingController _asociacion = TextEditingController();
  TextEditingController _comunidad = TextEditingController();
  TextEditingController _material = TextEditingController();
  TextEditingController _descripcion = TextEditingController();

  String name;
  String lastName;
  String direccion;
  int cell;
  String asociacion;
  String comunidad;
  String materiales;
  String descripcion;
  int radioValue2;
  bool pertenece = true;
  final _formKey = GlobalKey<FormState>();


  selected2(int val) {
    setState(() {
      radioValue2 = val;
    });
  }

  bool _validarlo() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _enviar() {
    if (_validarlo()) {
      setState(() {
        _isInAsyncCall = true;
      });
      FirebaseFirestore.instance
          .collection('artesano')
          .add({
            'id': 0,
            'name': name,
            'lastName': lastName,
            'direccion': direccion,
            'cell': cell,
            'asociacion': asociacion,
            'comunidad': comunidad,
            'materiales': materiales,
            'descripcion': descripcion
          })
          .then((value) => Navigator.of(context).pop())
          .catchError((onError) => print('Error al registrar su producto'));
      _isInAsyncCall = false;
      //}
    } else {
      print('objeto no validado');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          flexibleSpace: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width / 6, top: 20),
                  child: Image(
                    image: AssetImage('assets/images/logo2.png'),
                    width: 150,
                    height: 80,
                  )),
              Spacer(),
            ],
          ),
        ),
        body: ModalProgressHUD(
            inAsyncCall: _isInAsyncCall,
            opacity: 0.5,
            dismissible: false,
            progressIndicator: CircularProgressIndicator(),
            color: Colors.blueGrey,
            child: SingleChildScrollView(
                padding: EdgeInsets.only(left: 10, right: 15),
                child: Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                          borderRadius: BorderRadius.circular(10)),
                      margin: EdgeInsets.only(
                          top: 15, left: 15, right: 15, bottom: 15),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'FORMULARIO DE REGISTRO PARA ARTESANOS DE BOLIVIA',
                            style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Este formulario es epsecíficamente para registrar a ARTESANOS BOLIVIANOS que deseen vender sus productos a traves de esta plataforma. De no ser uno, absténgase de llenarlo. Los datos serán corroborados',
                            style: TextStyle(
                              color: Colors.blue.shade900,
                              fontFamily: 'Roboto',
                              fontSize: 12,
                              //fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                            ),
                            textAlign: TextAlign.justify,
                          ),
                          Text(
                            'Nombres',
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 12),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white.withOpacity(0.8)),
                            height: 25,
                            child: TextFormField(
                              controller: _nameArt,
                              textAlign: TextAlign.center,
                              autofocus: true,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.black.withOpacity(0.4),
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                                hintText: ' p.e. Julio Augusto',
                              ),
                              validator: (String val) {
                                if (val.isEmpty) {
                                  return 'Escriba algun texto';
                                }
                                return null;
                              },
                              onSaved: (value) => name = value.trim(),
                            ),
                          ),
                          Text(
                            'Apellidos',
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 12),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white.withOpacity(0.8)),
                            height: 25,
                            child: TextFormField(
                              controller: _apellido,
                              textAlign: TextAlign.center,
                              autofocus: true,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.black.withOpacity(0.4),
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                                hintText: ' p.e. Quispe Flores',
                              ),
                              validator: (String val) {
                                if (val.isEmpty) {
                                  return 'Escriba algun texto';
                                }
                                return null;
                              },
                              onSaved: (value) => lastName = value.trim(),
                            ),
                          ),
                          Text(
                            'Dirección actual (calle, #, zona u otro)',
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 12),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white.withOpacity(0.8)),
                            height: 25,
                            child: TextFormField(
                              controller: _direccion,
                              textAlign: TextAlign.center,
                              autofocus: true,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.black.withOpacity(0.4),
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                                hintText:
                                    ' p.e. Calle 6 de Agosto #10025, Zona Los Olivos',
                              ),
                              validator: (String val) {
                                if (val.isEmpty) {
                                  return 'Escriba algun texto';
                                }
                                return null;
                              },
                              onSaved: (value) => lastName = value.trim(),
                            ),
                          ),
                          Text(
                            'Numero de referencia para contactarnos contigo',
                            style:
                                TextStyle(fontFamily: 'Roboto', fontSize: 12),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white.withOpacity(0.8)),
                            height: 25,
                            child: TextFormField(
                              controller: _telefono,
                              textAlign: TextAlign.center,
                              autofocus: true,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.black.withOpacity(0.4),
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                                hintText: ' p.e. 65891278',
                              ),
                              validator: (String val) {
                                if (val.isEmpty) {
                                  return 'Escriba algun texto';
                                }
                                return null;
                              },
                              onSaved: (value) => cell = int.parse(value),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            child: Text(
                              '¿Pertenece a alguna asociación de artesanos?',
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                          Row(children: <Widget>[
                            Radio(
                              value: 1,
                              groupValue: radioValue2,
                              activeColor: Colors.green.shade600,
                              onChanged: (val) {
                                selected2(val);
                                pertenece = true;
                              },
                            ),
                            Text(
                              'SI',
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 12,
                              ),
                            )
                          ]),
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 2,
                                groupValue: radioValue2,
                                activeColor: Colors.green.shade600,
                                onChanged: (val) {
                                  selected2(val);
                                  pertenece = false;
                                },
                              ),
                              Text(
                                'NO',
                                style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                          Visibility(
                              visible: pertenece,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: double.infinity,
                                    child: Text(
                                      'Nombre de la Asociación de artesanos de tu comunidad',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white.withOpacity(0.8)),
                                    height: 25,
                                    child: TextFormField(
                                      controller: _asociacion,
                                      textAlign: TextAlign.center,
                                      autofocus: true,
                                      style: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: 15,
                                      ),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(
                                            color:
                                                Colors.black.withOpacity(0.4),
                                            fontFamily: 'Roboto',
                                            fontSize: 12),
                                        hintText:
                                            ' p.e. Asociacion de artesanos de Pacajes',
                                      ),
                                      validator: (String val) {
                                        if (val.isEmpty) {
                                          return 'Escriba algun texto';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) =>
                                          asociacion = value.trim(),
                                    ),
                                  ),
                                  Container(
                                    width: double.infinity,
                                    child: Text(
                                      'Nombre de tu comunidad, provincia u otro',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white.withOpacity(0.8)),
                                    height: 25,
                                    child: TextFormField(
                                      controller: _comunidad,
                                      textAlign: TextAlign.center,
                                      autofocus: true,
                                      style: TextStyle(
                                        fontFamily: 'Roboto',
                                        fontSize: 15,
                                      ),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(
                                            color:
                                                Colors.black.withOpacity(0.4),
                                            fontFamily: 'Roboto',
                                            fontSize: 12),
                                        hintText: ' p.e. Provincia Pacajes',
                                      ),
                                      validator: (String val) {
                                        if (val.isEmpty) {
                                          return 'Escriba algun texto';
                                        }
                                        return null;
                                      },
                                      onSaved: (value) =>
                                          comunidad = value.trim(),
                                    ),
                                  ),
                                ],
                              )),
                          Container(
                            width: double.infinity,
                            child: Text(
                              '¿Que materiales utilizas para realizar tus artesanías?',
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white.withOpacity(0.8)),
                            height: 25,
                            child: TextFormField(
                              controller: _material,
                              textAlign: TextAlign.center,
                              autofocus: true,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.black.withOpacity(0.4),
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                                hintText: ' p.e. cuero, aguayo y tela popelina',
                              ),
                              validator: (String val) {
                                if (val.isEmpty) {
                                  return 'Escriba algun texto';
                                }
                                return null;
                              },
                              onSaved: (value) => materiales = value.trim(),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            child: Text(
                              'Danos una breve descripcion de los tipos de Artesanias que realizas',
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white.withOpacity(0.8)),
                            height: 25,
                            child: TextFormField(
                              controller: _descripcion,
                              textAlign: TextAlign.center,
                              autofocus: true,
                              style: TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 15,
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    color: Colors.black.withOpacity(0.4),
                                    fontFamily: 'Roboto',
                                    fontSize: 12),
                                hintText:
                                    ' p.e. Realizo mochilas y carteras para dama',
                              ),
                              validator: (String val) {
                                if (val.isEmpty) {
                                  return 'Escriba algun texto';
                                }
                                return null;
                              },
                              onSaved: (value) => descripcion = value.trim(),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              RaisedButton(
                                onPressed: _enviar,
                                child: Text('Crear Producto',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Roboto',
                                        fontSize: 15)),
                                color: Colors.green,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ]),
                ))));
  }
}
