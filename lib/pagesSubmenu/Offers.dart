import 'dart:async';
import 'package:artbol/pages/ShopCar.dart';
import 'package:artbol/pagesSubmenu/firebaseOffExtInt.dart';
import 'package:artbol/pagesSubmenu/firebaseOffOtros.dart';
import 'package:artbol/pagesSubmenu/firebaseOffRZA.dart';
//import 'package:artbol/pagesSubmenu/firebaseService.dart';
import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:artbol/pagesSubmenu/update.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'artModel.dart';
import 'firebaseArtesanos.dart';
import 'firebaseOffert.dart';

class Offers extends StatefulWidget {
  static const routeOf = 'routeOfferts';
  @override
  _OffersState createState() => _OffersState();
}

class _OffersState extends State<Offers> {
  List<ProdModel> listaProd = List<ProdModel>();
  // List<ProdModel> _listaCar = [];
  FirebaseOffert db = new FirebaseOffert();
  StreamSubscription<QuerySnapshot> productSub;

  List<ArtModel> listaArt = List<ArtModel>();
  FirebaseArtesanos db2 = new FirebaseArtesanos();
  StreamSubscription<QuerySnapshot> artesanos;

  List<String> email = List<String>();

  deleteData(int id) async {
    CollectionReference prodColl =
        FirebaseFirestore.instance.collection('ofertas');
    QuerySnapshot querySnapshot = await prodColl.get();
    querySnapshot.docs[id].reference.delete();
  }

  @override
  void initState() {
    super.initState();
    listaProd = new List();
    listaArt = new List();
    artesanos?.cancel();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
    artesanos = db2.getListProd().listen((QuerySnapshot snapshot) {
      final List<ArtModel> artesanos = snapshot.docs
          .map((documentSnapshot) => ArtModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaArt = artesanos;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = (MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height));

    final user = Provider.of<UserRepository>(context, listen: false);
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: DefaultTabController(
            length: 4,
            child: DefaultTabController(
                length: 4,
                child: Scaffold(
                  appBar: AppBar(
                    backgroundColor: Colors.black,
                    flexibleSpace: Row(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width / 6,
                                top: 20),
                            child: Image(
                              image: AssetImage('assets/images/logo2.png'),
                              width: 150,
                              height: 80,
                            )),
                        Spacer(),
                        Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: ShopCar(user.listaCar))
                      ],
                    ),
                    bottom: TabBar(
                      indicatorColor: Colors.red,
                      indicatorSize: TabBarIndicatorSize.label,
                      tabs: <Widget>[
                        Tab(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: Text('COCINA',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 10)),
                          ),
                        ),
                        Tab(
                          child: Padding(
                              padding: EdgeInsets.only(top: 5),
                              child: Text('ROPA, ZAPATOS Y ACCESORIOS',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 10))),
                        ),
                        Tab(
                          child: Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: Text('DECORACION INTERIORES Y EXTERIORES',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 10)),
                          ),
                        ),
                        Tab(
                            child: Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: Text(
                            'OTROS',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 10),
                          ),
                        ))
                      ],
                    ),
                  ),
                  body: TabBarView(
                    children: <Widget>[
                      GridView.builder(
                        padding: EdgeInsets.all(5),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount:
                                (orientation == Orientation.portrait) ? 2 : 3,
                            childAspectRatio:
                                (orientation == Orientation.portrait)
                                    ? size2 + 0.15
                                    : size1 + 0.15),
                        itemCount: listaProd.length,
                        itemBuilder: (context, index) {
                          var item = listaProd[index];
                          if (user.notvalue) {
                            user.value = false;
                          } else {
                            for (var item in listaArt) {
                              email.add(item.id.toString());
                            }
                            if (email.contains(user.user.email.toString())) {
                              user.value = true;
                            } else {
                              user.value = false;
                            }
                          }

                          return Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.all(5),
                            elevation: 4,
                            color: Colors.red.shade900,
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.all(5),
                                    child: CachedNetworkImage(
                                        imageUrl: '${listaProd[index].image}' +
                                            '?alt=media',
                                        fit: BoxFit.contain,
                                        placeholder: (_, __) {
                                          return Center(
                                              child: CupertinoActivityIndicator(
                                            radius: 15,
                                          ));
                                        }),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 5, right: 5, bottom: 2),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Container(
                                          child: Flexible(
                                        fit: FlexFit.tight,
                                        child: Text(
                                          '${listaProd[index].name}',
                                          maxLines: 2,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              color: Colors.yellow,
                                              fontFamily: 'Roboto',
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ))
                                    ],
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(left: 5, right: 5),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Container(
                                            child: Flexible(
                                          fit: FlexFit.tight,
                                          child: Text(
                                            '${listaProd[index].descripcion}',
                                            maxLines: 3,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                              color:
                                                  Colors.white.withOpacity(0.9),
                                              fontFamily: 'Roboto',
                                              fontSize: 10,
                                            ),
                                          ),
                                        ))
                                      ],
                                    )),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Flexible(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            top: 5,
                                            left: 5,
                                            bottom: 10,
                                            right: 0),
                                        child: Text(
                                            '${listaProd[index].price}'
                                                    .toString() +
                                                ' Bs',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.yellow.shade300,
                                                fontFamily: 'Roboto',
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ),
                                    Visibility(
                                      visible: user.value,
                                      child: GestureDetector(
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, right: 0, bottom: 10),
                                            child: Icon(
                                              Icons.delete,
                                              color: Colors.black,
                                            ),
                                          ),
                                          onTap: () {
                                            setState(() {
                                              deleteData(index);
                                            });
                                          }),
                                    ),
                                    Visibility(
                                      visible: user.value,
                                      child: GestureDetector(
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 10, right: 0, bottom: 10),
                                          child: Icon(
                                            Icons.edit,
                                            color: Colors.black,
                                          ),
                                        ),
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(builder:
                                                  (BuildContext context) {
                                            return Update(index, 'ofertas');
                                          }));
                                        },
                                      ),
                                    ),
                                    Visibility(
                                      visible:
                                          (user.notvalue) ? false : !user.value,
                                      child: GestureDetector(
                                        child: (!user.listaCar.contains(item))
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    right: 5,
                                                    bottom: 10),
                                                child: Icon(
                                                  Icons.add_shopping_cart,
                                                  color: Colors.yellow.shade200,
                                                ),
                                              )
                                            : Padding(
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    right: 5,
                                                    bottom: 10),
                                                child: Icon(
                                                  Icons.add_shopping_cart,
                                                  color: Colors.cyan.shade200,
                                                ),
                                              ),
                                        onTap: () {
                                          setState(() {
                                            if (!user.listaCar.contains(item)) {
                                              print('sh');
                                              user.listaCar.add(item);
                                            } else {
                                              print('remove');
                                              user.listaCar.remove(item);
                                            }
                                          });
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      Product(),
                      Product2(),
                      Product3()
                    ],
                  ),
                ))));
  }
}

class Product extends StatefulWidget {
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  List<ProdModel> listaProd = List<ProdModel>();
  List<ProdModel> listaCar = [];
  FirebaseOffertRZA db = new FirebaseOffertRZA();
  StreamSubscription<QuerySnapshot> productSub;

  List<ArtModel> listaArt = List<ArtModel>();
  FirebaseArtesanos db2 = new FirebaseArtesanos();
  StreamSubscription<QuerySnapshot> artesanos;

  List<String> email = List<String>();

  deleteData(int id) async {
    CollectionReference prodColl =
        FirebaseFirestore.instance.collection('ofertasRZA');
    QuerySnapshot querySnapshot = await prodColl.get();
    querySnapshot.docs[id].reference.delete();
  }

  @override
  void initState() {
    super.initState();
    listaProd = new List();
    listaArt = new List();
    artesanos?.cancel();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
    artesanos = db2.getListProd().listen((QuerySnapshot snapshot) {
      final List<ArtModel> artesanos = snapshot.docs
          .map((documentSnapshot) => ArtModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaArt = artesanos;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height);

    return GridView.builder(
      padding: EdgeInsets.all(5),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
          childAspectRatio:
              (orientation == Orientation.portrait) ? size2 + 0.15 : size1),
      itemCount: listaProd.length,
      itemBuilder: (context, index) {
        var item = listaProd[index];
        if (user.notvalue) {
          user.value = false;
        } else {
          for (var item in listaArt) {
            email.add(item.id.toString());
          }
          if (email.contains(user.user.email.toString())) {
            user.value = true;
          } else {
            user.value = false;
          }
        }

        return Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.all(5),
          elevation: 4,
          color: Colors.red.shade900,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: CachedNetworkImage(
                      imageUrl: '${listaProd[index].image}' + '?alt=media',
                      fit: BoxFit.contain,
                      placeholder: (_, __) {
                        return Center(
                            child: CupertinoActivityIndicator(
                          radius: 15,
                        ));
                      }),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5, right: 5, bottom: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                        child: Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        '${listaProd[index].name}',
                        maxLines: 2,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: Colors.yellow,
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold),
                      ),
                    ))
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                          child: Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          '${listaProd[index].descripcion}',
                          maxLines: 3,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.9),
                            fontFamily: 'Roboto',
                            fontSize: 10,
                          ),
                        ),
                      ))
                    ],
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 5, left: 15, bottom: 10, right: 0),
                      child: Text(
                          '${listaProd[index].price}'.toString() + ' Bs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.yellow.shade300,
                              fontFamily: 'Roboto',
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Visibility(
                    visible: user.value,
                    child: GestureDetector(
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 10, right: 0, bottom: 10),
                          child: Icon(
                            Icons.delete,
                            color: Colors.black,
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            deleteData(index);
                          });
                        }),
                  ),
                  Visibility(
                    visible: user.value,
                    child: GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(top: 10, right: 0, bottom: 10),
                        child: Icon(
                          Icons.edit,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Update(index, 'ofertasRZA');
                        }));
                      },
                    ),
                  ),
                  Visibility(
                      visible: (user.notvalue) ? false : !user.value,
                      child: GestureDetector(
                        child: (!user.listaCar.contains(item))
                            ? Padding(
                                padding: EdgeInsets.only(
                                    top: 10, right: 15, bottom: 10),
                                child: Icon(
                                  Icons.add_shopping_cart,
                                  color: Colors.yellow.shade200,
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(
                                    top: 10, right: 15, bottom: 10),
                                child: Icon(Icons.add_shopping_cart,
                                    color: Colors.cyan.shade200),
                              ),
                        onTap: () {
                          setState(() {
                            if (!user.listaCar.contains(item)) {
                              user.listaCar.add(item);
                            } else {
                              user.listaCar.remove(item);
                            }
                          });
                        },
                      ))
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

class Product2 extends StatefulWidget {
  @override
  _Product2State createState() => _Product2State();
}

class _Product2State extends State<Product2> {
  List<ProdModel> listaProd = List<ProdModel>();
  List<ProdModel> listaCar = [];
  FirebaseOffertExtInt db = new FirebaseOffertExtInt();
  StreamSubscription<QuerySnapshot> productSub;
  List<ArtModel> listaArt = List<ArtModel>();
  FirebaseArtesanos db2 = new FirebaseArtesanos();
  StreamSubscription<QuerySnapshot> artesanos;

  List<String> email = List<String>();

  deleteData(int id) async {
    CollectionReference prodColl =
        FirebaseFirestore.instance.collection('ofDecExtInt');
    QuerySnapshot querySnapshot = await prodColl.get();
    querySnapshot.docs[id].reference.delete();
  }

  @override
  void initState() {
    super.initState();
    listaProd = new List();
    listaArt = new List();
    artesanos?.cancel();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
    artesanos = db2.getListProd().listen((QuerySnapshot snapshot) {
      final List<ArtModel> artesanos = snapshot.docs
          .map((documentSnapshot) => ArtModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaArt = artesanos;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height);

    return GridView.builder(
      padding: EdgeInsets.all(5),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
          childAspectRatio:
              (orientation == Orientation.portrait) ? size2 + 0.15 : size1),
      itemCount: listaProd.length,
      itemBuilder: (context, index) {
        var item = listaProd[index];
        if (user.notvalue) {
          user.value = false;
        } else {
          for (var item in listaArt) {
            email.add(item.id.toString());
          }
          if (email.contains(user.user.email.toString())) {
            user.value = true;
          } else {
            user.value = false;
          }
        }
        return Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.all(5),
          elevation: 4,
          color: Colors.red.shade900,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: CachedNetworkImage(
                      imageUrl: '${listaProd[index].image}' + '?alt=media',
                      fit: BoxFit.contain,
                      placeholder: (_, __) {
                        return Center(
                            child: CupertinoActivityIndicator(
                          radius: 15,
                        ));
                      }),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5, right: 5, bottom: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                        child: Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        '${listaProd[index].name}',
                        maxLines: 2,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: Colors.yellow,
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold),
                      ),
                    ))
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                          child: Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          '${listaProd[index].descripcion}',
                          maxLines: 3,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.9),
                            fontFamily: 'Roboto',
                            fontSize: 10,
                          ),
                        ),
                      ))
                    ],
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 5, left: 15, bottom: 10, right: 0),
                      child: Text(
                          '${listaProd[index].price}'.toString() + ' Bs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.yellow.shade300,
                              fontFamily: 'Roboto',
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Visibility(
                    visible: user.value,
                    child: GestureDetector(
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 10, right: 0, bottom: 10),
                          child: Icon(
                            Icons.delete,
                            color: Colors.black,
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            deleteData(index);
                          });
                        }),
                  ),
                  Visibility(
                    visible: user.value,
                    child: GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(top: 10, right: 0, bottom: 10),
                        child: Icon(
                          Icons.edit,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Update(index, 'ofDecExtInt');
                        }));
                      },
                    ),
                  ),
                  Visibility(
                    visible: (user.notvalue) ? false : !user.value,
                    child: GestureDetector(
                        child: (!user.listaCar.contains(item))
                            ? Padding(
                                padding: EdgeInsets.only(
                                    top: 10, right: 15, bottom: 10),
                                child: Icon(
                                  Icons.add_shopping_cart,
                                  color: Colors.yellow.shade200,
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(
                                    top: 10, right: 15, bottom: 10),
                                child: Icon(Icons.add_shopping_cart,
                                    color: Colors.cyan.shade200),
                              ),
                        onTap: () {
                          setState(() {
                            if (!user.listaCar.contains(item)) {
                              user.listaCar.add(item);
                            } else {
                              user.listaCar.remove(item);
                            }
                          });
                        }),
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

class Product3 extends StatefulWidget {
  @override
  _Product3State createState() => _Product3State();
}

class _Product3State extends State<Product3> {
  List<ProdModel> listaProd = List<ProdModel>();
  List<ProdModel> listaCar = [];
  FirebaseOffertOtros db = new FirebaseOffertOtros();
  StreamSubscription<QuerySnapshot> productSub;

  List<ArtModel> listaArt = List<ArtModel>();
  FirebaseArtesanos db2 = new FirebaseArtesanos();
  StreamSubscription<QuerySnapshot> artesanos;

  List<String> email = List<String>();
  deleteData(int id) async {
    CollectionReference prodColl =
        FirebaseFirestore.instance.collection('ofertasOtros');
    QuerySnapshot querySnapshot = await prodColl.get();
    querySnapshot.docs[id].reference.delete();
  }

  @override
  void initState() {
    super.initState();
    listaProd = new List();
    listaArt = new List();
    artesanos?.cancel();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
    artesanos = db2.getListProd().listen((QuerySnapshot snapshot) {
      final List<ArtModel> artesanos = snapshot.docs
          .map((documentSnapshot) => ArtModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaArt = artesanos;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height);

    return GridView.builder(
      padding: EdgeInsets.all(5),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
          childAspectRatio:
              (orientation == Orientation.portrait) ? size2 + 0.15 : size1),
      itemCount: listaProd.length,
      itemBuilder: (context, index) {
        var item = listaProd[index];
        if (user.notvalue) {
          user.value = false;
        } else {
          for (var item in listaArt) {
            email.add(item.id.toString());
          }
          if (email.contains(user.user.email.toString())) {
            user.value = true;
          } else {
            user.value = false;
          }
        }
        return Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.all(5),
          elevation: 4,
          color: Colors.red.shade900,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: CachedNetworkImage(
                      imageUrl: '${listaProd[index].image}' + '?alt=media',
                      fit: BoxFit.contain,
                      placeholder: (_, __) {
                        return Center(
                            child: CupertinoActivityIndicator(
                          radius: 15,
                        ));
                      }),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5, right: 5, bottom: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                        child: Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        '${listaProd[index].name}',
                        maxLines: 2,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            color: Colors.yellow,
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold),
                      ),
                    ))
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                          child: Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          '${listaProd[index].descripcion}',
                          maxLines: 3,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.9),
                            fontFamily: 'Roboto',
                            fontSize: 10,
                          ),
                        ),
                      ))
                    ],
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 5, left: 15, bottom: 10, right: 0),
                      child: Text(
                          '${listaProd[index].price}'.toString() + ' Bs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.yellow.shade300,
                              fontFamily: 'Roboto',
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Visibility(
                    visible: user.value,
                    child: GestureDetector(
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 10, right: 0, bottom: 10),
                          child: Icon(
                            Icons.delete,
                            color: Colors.black,
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            deleteData(index);
                          });
                        }),
                  ),
                  Visibility(
                    visible: user.value,
                    child: GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(top: 10, right: 0, bottom: 10),
                        child: Icon(
                          Icons.edit,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (BuildContext context) {
                          return Update(index, 'ofertasOtros');
                        }));
                      },
                    ),
                  ),
                  Visibility(
                      visible: (user.notvalue) ? false : !user.value,
                      child: GestureDetector(
                        child: (!user.listaCar.contains(item))
                            ? Padding(
                                padding: EdgeInsets.only(
                                    top: 10, right: 15, bottom: 10),
                                child: Icon(
                                  Icons.add_shopping_cart,
                                  color: Colors.yellow.shade200,
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(
                                    top: 10, right: 15, bottom: 10),
                                child: Icon(Icons.add_shopping_cart,
                                    color: Colors.cyan.shade200),
                              ),
                        onTap: () {
                          setState(() {
                            if (!user.listaCar.contains(item)) {
                              //ShopCar(listaCar.add(item));
                              print('sh');
                              user.listaCar.add(item);
                            } else {
                              print('remove');
                              user.listaCar.remove(item);
                            }
                          });
                        },
                      ))
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
