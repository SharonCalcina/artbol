//import 'package:artbol/pages/Home.dart';
import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:artbol/side_bar/Register.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:fancy_dialog/FancyAnimation.dart';
//import 'package:fancy_dialog/FancyGif.dart';
import 'package:fancy_dialog/FancyTheme.dart';
import 'package:fancy_dialog/fancy_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class Cart extends StatefulWidget {
  final List<ProdModel> _cart;
  Cart(this._cart);
  @override
  _CartState createState() => _CartState(this._cart);
}

class _CartState extends State<Cart> {
  _CartState(this._cart);
  final _scrollController = ScrollController();
  var _firstScroll = true;
  bool _enabled = false;

  List<ProdModel> _cart;

  Container pagoTotal(List<ProdModel> _cart) {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(left: 120),
      height: 70,
      width: 400,
      color: Colors.grey[200],
      child: Row(
        children: <Widget>[
          Text("Total:  \$${valorTotal(_cart)}",
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                  color: Colors.black))
        ],
      ),
    );
  }

  String valorTotal(_cart) {
    double total = 0.0;

    for (int i = 0; i < _cart.length; i++) {
      total = total + _cart[i].price * _cart[i].quantity;
    }
    return total.toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    return (user.notvalue)
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.black,
              flexibleSpace: Row(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 6, top: 20),
                      child: Image(
                        image: AssetImage('assets/images/logo2.png'),
                        width: 150,
                        height: 80,
                      )),
                ],
              ),
            ),
            body: Column(
              children: <Widget>[
                SizedBox(height: 20,),
                Icon(Icons.account_circle, size: 100,),
                Container(
                  margin: EdgeInsets.all(40),
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.grey.shade300),
                  child: Text(
                    'Tu carrito esta vacio. Para comprar debes iniciar sesion. Si eres nuevo crea una cuenta',
                    style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 15,
                        color: Colors.black),
                  ),
                ),
                RaisedButton(
                    color: Colors.red.shade600,
                    child: Text(
                      'Crear cuenta o Iniciar Sesion',
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(Register.routeReg);
                    }),
                    
              ],
            ))
        : ChangeNotifierProvider(
            create: (context) => UserRepository.instance(),
            child: Scaffold(
              appBar: AppBar(
                actions: <Widget>[],
                title: Text('DETALLE',
                    style: new TextStyle(
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                        color: Colors.black)),
                centerTitle: true,
                leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                    setState(() {
                      _cart.length;
                    });
                  },
                  color: Colors.white,
                ),
                backgroundColor: Colors.transparent,
              ),
              body: GestureDetector(
                  onVerticalDragUpdate: (details) {
                    if (_enabled && _firstScroll) {
                      _scrollController.jumpTo(
                          _scrollController.position.pixels - details.delta.dy);
                    }
                  },
                  onVerticalDragEnd: (_) {
                    if (_enabled) _firstScroll = false;
                  },
                  child: SingleChildScrollView(
                      child: Column(
                    children: <Widget>[
                      ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: _cart.length,
                        itemBuilder: (context, index) {
                          //final String imagen = _cart[index].image;
                          var item = _cart[index];
                          return Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 2.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Container(
                                          width: 150,
                                          height: 150,
                                          child: CachedNetworkImage(
                                              imageUrl: '${item.image}' +
                                                  '?alt=media',
                                              fit: BoxFit.contain,
                                              placeholder: (_, __) {
                                                return Center(
                                                    child:
                                                        CupertinoActivityIndicator(
                                                  radius: 15,
                                                ));
                                              }),
                                        )),
                                        SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                10),
                                        Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(5),
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      2 -
                                                  90,
                                              height: 100,
                                              child: Text(item.name,
                                                  textAlign: TextAlign.justify,
                                                  style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 12.5,
                                                      color: Colors.black)),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  width: 120,
                                                  height: 40,
                                                  decoration: BoxDecoration(
                                                      color: Colors.blue[900],
                                                      boxShadow: [
                                                        BoxShadow(
                                                          blurRadius: 6.0,
                                                          color: Colors
                                                              .orange[400],
                                                          offset:
                                                              Offset(0.0, 1.0),
                                                        )
                                                      ],
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(50.0),
                                                      )),
                                                  margin: EdgeInsets.only(
                                                      top: 20.0),
                                                  padding: EdgeInsets.all(2.0),
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: 8.0,
                                                      ),
                                                      IconButton(
                                                        icon:
                                                            Icon(Icons.remove),
                                                        onPressed: () {
                                                          setState(() {
                                                            _cart[index]
                                                                .quantity--;
                                                            valorTotal(_cart);
                                                          });
                                                        },
                                                        color: Colors.yellow,
                                                      ),
                                                      Text(
                                                          '${_cart[index].quantity}',
                                                          style: new TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 15,
                                                              color: Colors
                                                                  .white)),
                                                      IconButton(
                                                        icon: Icon(Icons.add),
                                                        onPressed: () {
                                                          setState(() {
                                                            _cart[index]
                                                                .quantity++;
                                                            valorTotal(_cart);
                                                          });
                                                        },
                                                        color: Colors.yellow,
                                                      ),
                                                      SizedBox(
                                                        height: 8.0,
                                                      )
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        Text(item.price.toString(),
                                            style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0,
                                                color: Colors.black))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Divider(
                                color: Colors.purple,
                              )
                            ],
                          );
                        },
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      pagoTotal(_cart),
                      SizedBox(
                        width: 20.0,
                      ),
                      Container(
                        height: 100,
                        width: 200,
                        padding: EdgeInsets.only(top: 50),
                        child: RaisedButton(
                          textColor: Colors.white,
                          color: Colors.green,
                          child: Text("PAGAR"),
                          onPressed: () => {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => FancyDialog(
                                      title: "Aceptar compra",
                                      descreption: "Enviar por WhatsApp",
                                      animationType: FancyAnimation.BOTTOM_TOP,
                                      theme: FancyTheme.FANCY,
                                      gifPath: './assets/images/buy2.gif',
                                      okFun: () => {
                                        Navigator.pop(context),
                                        msgListaPedido(),
                                        // Navigator.pushNamed(context, HomePage.routeHome),
                                        user.listCar.clear(),
                                      },
                                    )),
                            //Navigator.pop(context),
                          },
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                    ],
                  ))),
            ));
  }

  void msgListaPedido() async {
    String pedido = "";
    String fecha = DateTime.now().toString();
    pedido = pedido + "FECHA:" + fecha.toString();
    pedido = pedido + "\n";
    pedido = pedido + "MEGA DESCUENTOS A DOMICILIO";
    pedido = pedido + "\n";
    pedido = pedido + "CLIENTE: FLUTTER - DART";
    pedido = pedido + "\n";
    pedido = pedido + "_____________";

    for (int i = 0; i < _cart.length; i++) {
      pedido = '$pedido' +
          "\n" +
          "Producto : " +
          _cart[i].name +
          "\n" +
          "Cantidad: " +
          _cart[i].quantity.toString() +
          "\n" +
          "Precio : " +
          _cart[i].price.toString() +
          "\n" +
          "\_________________________\n";
    }
    pedido = pedido + "TOTAL:" + valorTotal(_cart);

    await launch("https://wa.me/${59178799034}?text=$pedido");
  }
}
