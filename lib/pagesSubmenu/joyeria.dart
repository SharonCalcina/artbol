import 'dart:async';
import 'package:artbol/pages/ShopCar.dart';
import 'package:artbol/pagesSubmenu/firebaseJoyeria.dart';
//import 'package:artbol/pagesSubmenu/firebaseService.dart';
import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'artModel.dart';
import 'firebaseArtesanos.dart';
import 'update.dart';

class Joyeria extends StatefulWidget {
  @override
  _JoyeriaState createState() => _JoyeriaState();
}

class _JoyeriaState extends State<Joyeria> {
  List<ProdModel> listaProd = List<ProdModel>();
  //List<ProdModel> _listaCar = [];
  FirebaseJoyeria db = new FirebaseJoyeria();
  StreamSubscription<QuerySnapshot> productSub;
  bool st1 = false, st2 = false, st3 = false, st4 = false, st5 = false;
  List<ArtModel> listaArt = List<ArtModel>();
  FirebaseArtesanos db2 = new FirebaseArtesanos();
  StreamSubscription<QuerySnapshot> artesanos;

  List<String> email = List<String>();

  deleteData(int id) async {
    CollectionReference prodColl =
        FirebaseFirestore.instance.collection('joyeria');
    QuerySnapshot querySnapshot = await prodColl.get();
    querySnapshot.docs[id].reference.delete();
  }

  @override
  void initState() {
    super.initState();
    listaProd = new List();
    listaArt = new List();
    artesanos?.cancel();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
    artesanos = db2.getListProd().listen((QuerySnapshot snapshot) {
      final List<ArtModel> artesanos = snapshot.docs
          .map((documentSnapshot) => ArtModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaArt = artesanos;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserRepository>(context, listen: false);
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = (MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height));
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: DefaultTabController(
            length: 1,
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.black,
                flexibleSpace: Row(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 6,
                            top: 20),
                        child: Image(
                          image: AssetImage('assets/images/logo2.png'),
                          width: 150,
                          height: 80,
                        )),
                    Spacer(),
                    Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: ShopCar(user.listaCar))
                  ],
                ),
                bottom: TabBar(
                  indicatorColor: Colors.orange,
                  indicatorSize: TabBarIndicatorSize.label,
                  tabs: <Widget>[
                    new Tab(
                      child: Padding(
                          padding: EdgeInsets.only(top: 5),
                          child: Text('ARTESANIAS EN JOYERIA',
                              //'COCINA',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'Roboto',
                                  fontWeight: FontWeight.bold))
                          /*child: Text(
                            'JOYAS DE ORO',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 10),
                          )*/
                          ),
                    ),
                    /*Tab(
                      child: Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text('JOYAS DE PLATA',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 10)),
                      ),
                    ),
                    Tab(
                      child: Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text('JOYAS DE ACERO',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 10)),
                      ),
                    ),
                    Tab(
                      child: Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text('OTROS',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 10)),
                      ),
                    )*/
                  ],
                ),
              ),
              body: TabBarView(
                children: <Widget>[
                  GridView.builder(
                    padding: EdgeInsets.all(5),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount:
                            (orientation == Orientation.portrait) ? 2 : 3,
                        childAspectRatio: (orientation == Orientation.portrait)
                            ? size2 + 0.15
                            : size1 + 0.15),
                    itemCount: listaProd.length,
                    itemBuilder: (context, index) {
                      var item = listaProd[index];
                      if (user.notvalue) {
                        user.value = false;
                      } else {
                        for (var item in listaArt) {
                          email.add(item.id.toString());
                        }
                        if (email.contains(user.user.email.toString())) {
                          user.value = true;
                        } else {
                          user.value = false;
                        }
                      }
                      return Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAlias,
                        margin: EdgeInsets.all(5),
                        elevation: 4,
                        color: Colors.grey.shade300,
                        child: Column(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                  padding: EdgeInsets.all(5),
                                  child: CachedNetworkImage(
                                      imageUrl: '${listaProd[index].image}' +
                                          '?alt=media',
                                      fit: BoxFit.contain,
                                      placeholder: (_, __) {
                                        return Center(
                                            child: CupertinoActivityIndicator(
                                          radius: 15,
                                        ));
                                      })),
                            ),
                            Row(children: <Widget>[
                              GestureDetector(
                                child: (!st1)
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, left: 5, bottom: 10),
                                        child: Icon(
                                          Icons.star_border,
                                          color: Colors.black,
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, left: 5, bottom: 10),
                                        child: Icon(Icons.star,
                                            color: Colors.green.shade800),
                                      ),
                                onTap: () {
                                  setState(() {
                                    st1 = true;
                                    st2 = false;
                                    st3 = false;
                                    st4 = false;
                                    st5 = false;
                                  });
                                },
                              ),
                              GestureDetector(
                                child: (!st2)
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(
                                          Icons.star_border,
                                          color: Colors.black,
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(Icons.star,
                                            color: Colors.green.shade800),
                                      ),
                                onTap: () {
                                  setState(() {
                                    st1 = true;
                                    st2 = true;
                                    st3 = false;
                                    st4 = false;
                                    st5 = false;
                                  });
                                },
                              ),
                              GestureDetector(
                                child: (!st3)
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(
                                          Icons.star_border,
                                          color: Colors.black,
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(Icons.star,
                                            color: Colors.green.shade800),
                                      ),
                                onTap: () {
                                  setState(() {
                                    st1 = true;
                                    st2 = true;
                                    st3 = true;
                                    st4 = false;
                                    st5 = false;
                                  });
                                },
                              ),
                              GestureDetector(
                                child: (!st4)
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(
                                          Icons.star_border,
                                          color: Colors.black,
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(Icons.star,
                                            color: Colors.green.shade800),
                                      ),
                                onTap: () {
                                  setState(() {
                                    st1 = true;
                                    st2 = true;
                                    st3 = true;
                                    st4 = true;
                                    st5 = false;
                                  });
                                },
                              ),
                              GestureDetector(
                                child: (!st5)
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(
                                          Icons.star_border,
                                          color: Colors.black,
                                        ),
                                      )
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Icon(Icons.star,
                                            color: Colors.green.shade800),
                                      ),
                                onTap: () {
                                  setState(() {
                                    st1 = true;
                                    st2 = true;
                                    st3 = true;
                                    st4 = true;
                                    st5 = true;
                                  });
                                },
                              ),
                            ]),
                            Padding(
                              padding:
                                  EdgeInsets.only(left: 5, right: 5, bottom: 2),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Container(
                                      child: Flexible(
                                    fit: FlexFit.tight,
                                    child: Text(
                                      '${listaProd[index].name}',
                                      maxLines: 2,
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ))
                                ],
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.only(left: 5, right: 5),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Container(
                                        child: Flexible(
                                      fit: FlexFit.tight,
                                      child: Text(
                                        '${listaProd[index].descripcion}',
                                        maxLines: 3,
                                        textAlign: TextAlign.justify,
                                        style: TextStyle(
                                          fontFamily: 'Roboto',
                                          fontSize: 10,
                                        ),
                                      ),
                                    ))
                                  ],
                                )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Flexible(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: 5, left: 5, bottom: 10, right: 0),
                                    child: Text(
                                        '${listaProd[index].price}'.toString() +
                                            ' Bs',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Roboto',
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                ),
                                Visibility(
                                  visible: user.value,
                                  child: GestureDetector(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 10, right: 0, bottom: 10),
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.red.shade800,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        deleteData(index);
                                      });
                                    },
                                  ),
                                ),
                                Visibility(
                                  visible: user.value,
                                  child: GestureDetector(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 10, right: 0, bottom: 10),
                                      child: Icon(
                                        Icons.edit,
                                        color: Colors.blue.shade800,
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) {
                                        return Update(index, 'joyeria');
                                      }));
                                    },
                                  ),
                                ),
                                Visibility(
                                  visible:
                                      (user.notvalue) ? false : !user.value,
                                  child: GestureDetector(
                                    child: (!user.listaCar.contains(item))
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, right: 5, bottom: 10),
                                            child: Icon(
                                              Icons.add_shopping_cart,
                                              color: Colors.black,
                                            ),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, right: 5, bottom: 10),
                                            child: Icon(Icons.add_shopping_cart,
                                                color: Colors.green),
                                          ),
                                    onTap: () {
                                      setState(() {
                                        if (!user.listaCar.contains(item)) {
                                          print('sh');
                                          user.listaCar.add(item);
                                        } else {
                                          print('remove');
                                          user.listaCar.remove(item);
                                        }
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  //Product(),
                  //Product(),
                  //Product(),
                ],
              ),
            )));
  }
}
/*
class Product extends StatefulWidget {
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  List<ProdModel> listaProd = List<ProdModel>();
  List<ProdModel> listaCar = [];
  FirebaseService db = new FirebaseService();
  StreamSubscription<QuerySnapshot> productSub;

  @override
  void initState() {
    super.initState();
    listaProd = new List();
    productSub?.cancel();
    productSub = db.getListProd().listen((QuerySnapshot snapshot) {
      final List<ProdModel> products = snapshot.docs
          .map((documentSnapshot) => ProdModel.fromMap(documentSnapshot.data()))
          .toList();
      setState(() {
        listaProd = products;
      });
    });
  }

  @override
  void dispose() {
    productSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final size1 = MediaQuery.of(context).size.height /
        (MediaQuery.of(context).size.width);
    final size2 = MediaQuery.of(context).size.width /
        (MediaQuery.of(context).size.height);

    return GridView.builder(
      padding: EdgeInsets.all(5),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
          childAspectRatio:
              (orientation == Orientation.portrait) ? size2 + 0.15 : size1),
      itemCount: listaProd.length,
      itemBuilder: (context, index) {
        var item = listaProd[index];

        return Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.all(5),
          elevation: 4,
          color: Colors.orange.shade400,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: CachedNetworkImage(
                      imageUrl: '${listaProd[index].image}' + '?alt=media',
                      fit: BoxFit.contain,
                      placeholder: (_, __) {
                        return Center(
                            child: CupertinoActivityIndicator(
                          radius: 15,
                        ));
                      }),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 5, right: 5, bottom: 2),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                        child: Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        '${listaProd[index].name}',
                        maxLines: 2,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 10,
                            fontWeight: FontWeight.bold),
                      ),
                    ))
                  ],
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                          child: Flexible(
                        fit: FlexFit.tight,
                        child: Text(
                          '${listaProd[index].descripcion}',
                          maxLines: 3,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 10,
                          ),
                        ),
                      ))
                    ],
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: 5, left: 15, bottom: 10, right: 0),
                      child: Text(
                          '${listaProd[index].price}'.toString() + ' Bs',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  GestureDetector(
                    child: (!Provider.of<UserRepository>(context, listen: false)
                            .listaCar
                            .contains(item))
                        ? Padding(
                            padding:
                                EdgeInsets.only(top: 10, right: 15, bottom: 10),
                            child: Icon(
                              Icons.add_shopping_cart,
                              color: Colors.black,
                            ),
                          )
                        : Padding(
                            padding:
                                EdgeInsets.only(top: 10, right: 15, bottom: 10),
                            child: Icon(Icons.add_shopping_cart,
                                color: Colors.red),
                          ),
                    onTap: () {
                      setState(() {
                        if (!Provider.of<UserRepository>(context, listen: false)
                            .listaCar
                            .contains(item)) {
                          //ShopCar(listaCar.add(item));
                          print('sh');
                          Provider.of<UserRepository>(context, listen: false)
                              .listaCar
                              .add(item);
                        } else {
                          print('remove');
                          Provider.of<UserRepository>(context, listen: false)
                              .listaCar
                              .remove(item);
                        }
                      });
                    },
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}*/
