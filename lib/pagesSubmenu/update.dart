import 'dart:async';
import 'package:artbol/pagesSubmenu/prod_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'firebaseOffert.dart';
import 'firebaseService.dart';

class CommonThings {
  static Size size;
}

class Update extends StatefulWidget {
  final String _page;
  final int _id;
  const Update(this._id, this._page);

  @override
  _UpdateState createState() => _UpdateState(this._id, this._page);
}

enum SelectSource { camara, galeria }

class _UpdateState extends State<Update> {
  _UpdateState(this._id, this._page);
  int _id;
  String _page;
  int price;
  List<ProdModel> listaProd = List<ProdModel>();

  FirebaseService db1 = new FirebaseService();
  FirebaseOffert db2 = new FirebaseOffert();
  StreamSubscription<QuerySnapshot> productSub;

  //Auth auth = Auth();

  TextEditingController priceInputController;
  TextEditingController nameInputController;
  TextEditingController imageInputController;
  TextEditingController descriptionInputController;
  bool _isInAsyncCall = false;

  String id;
  final db = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();
  String name;
  String desc;
  String uid;
  @override
  void initState() {
    super.initState();
    listaProd = new List();
    productSub?.cancel();
    if (_page == 'productos') {
      productSub = db1.getListProd().listen((QuerySnapshot snapshot) {
        final List<ProdModel> products = snapshot.docs
            .map((documentSnapshot) =>
                ProdModel.fromMap(documentSnapshot.data()))
            .toList();
        setState(() {
          listaProd = products;
        });
      });
    }
    if (_page == 'ofertas') {
      productSub = db2.getListProd().listen((QuerySnapshot snapshot) {
        final List<ProdModel> products = snapshot.docs
            .map((documentSnapshot) =>
                ProdModel.fromMap(documentSnapshot.data()))
            .toList();
        setState(() {
          listaProd = products;
        });
      });
    }
  }

  Widget divider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
      child: Container(
        width: 0.8,
        color: Colors.black,
      ),
    );
  }

  bool _validarlo() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  updateData() async {
    if (_validarlo()) {
      setState(() {
        _isInAsyncCall = false;
      });

      CollectionReference prodColl =
          FirebaseFirestore.instance.collection(_page);
      QuerySnapshot querySnapshot = await prodColl.get();
      querySnapshot.docs[_id].reference
          .update({
            'name': this.name,
            'price': this.price,
            'desc': this.desc,
          })
          .then((value) => Navigator.of(context).pop())
          .catchError(
              (onError) => print('Error al actualizar su producto en la bd'));
    } else {
      print('producto no validado');
    }
  }

  /*deleteData() async {
    CollectionReference prodColl =
        FirebaseFirestore.instance.collection('productos');
    QuerySnapshot querySnapshot = await prodColl.get();
    querySnapshot.docs[_id].reference.delete();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          flexibleSpace: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width / 6, top: 20),
                  child: Image(
                    image: AssetImage('assets/images/logo2.png'),
                    width: 150,
                    height: 80,
                  )),
              Spacer(),
            ],
          ),
        ),
        body: ModalProgressHUD(
          inAsyncCall: _isInAsyncCall,
          opacity: 0.5,
          dismissible: false,
          progressIndicator: CircularProgressIndicator(),
          color: Colors.blueGrey,
          child: SingleChildScrollView(
            padding: EdgeInsets.only(left: 10, right: 15),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 20, bottom: 20, top: 60),
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width / 2 - 40,
                        child: Padding(
                          padding: EdgeInsets.all(5),
                          child: CachedNetworkImage(
                              imageUrl:
                                  '${listaProd[_id].image}' + '?alt=media',
                              fit: BoxFit.contain,
                              placeholder: (_, __) {
                                return Center(
                                    child: CupertinoActivityIndicator(
                                  radius: 15,
                                ));
                              }),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      initialValue: '${listaProd[_id].name}',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 15),
                      maxLines: 2,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'nombre',
                        //fillColor: Colors.grey[300],
                        filled: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingrese algun nombre';
                        }
                        return null;
                      },
                      onSaved: (value) => this.name = value.trim(),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      initialValue: '${listaProd[_id].descripcion}',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 15),
                      maxLines: 3,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'decripción',
                        //fillColor: Colors.grey[300],
                        filled: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingresa alguna descripción';
                        }
                        return null;
                      },
                      onSaved: (value) => desc = value.trim(),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      initialValue: '${listaProd[_id].price}',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 15),
                      maxLines: 1,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'precio en Bs',
                        //fillColor: Colors.grey[300],
                        filled: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingresa algún precio';
                        }
                        return null;
                      },
                      onSaved: (value) => price = int.parse(value),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          updateData();
                        },
                        child: Text('Actualizar Producto',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Roboto',
                                fontSize: 15)),
                        color: Colors.green,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
