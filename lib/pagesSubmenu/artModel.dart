class ArtModel {
  var id;
  String name;
  String lastName;
  String direccion;
  int cell;
  String asociacion;
  String comunidad;
  String materiales;
  String descripcion;

  ArtModel(
      String docId,
      String name,
      String lastName,
      String direccion,
      int cell,
      String asociacion,
      String comunidad,
      String materiales,
      String descripcion);
  ArtModel.map(dynamic obj) {
    this.id = obj['id'];
    this.name = obj['name'];
    this.lastName = obj['lastName'];
    this.direccion = obj['direccion'];
    this.cell = obj['cell'];
    this.asociacion = obj['asociacion'];
    this.comunidad = obj['comunidad'];
    this.materiales = obj['materiales'];
    this.descripcion = obj['descripcion'];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['lastName'] = lastName;
    map['direccion'] = direccion;
    map['cell'] = cell;
    map['asociacion'] = asociacion;
    map['comunidad'] = comunidad;
    map['materiales'] = materiales;
    map['descripcion'] = descripcion;
    return map;
  }

  ArtModel.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.name = map['name'];
    this.lastName = map['lastName'];
    this.direccion = map['direccion'];
    this.cell = map['cell'];
    this.asociacion = map['asociacion'];
    this.comunidad = map['comunidad'];
    this.materiales = map['materiales'];
    this.descripcion = map['descripcion'];
  }
}
