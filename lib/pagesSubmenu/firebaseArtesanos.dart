import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

import 'artModel.dart';

final CollectionReference artColl =
    FirebaseFirestore.instance.collection('artesano');

class FirebaseArtesanos {
  static final FirebaseArtesanos _instance = new FirebaseArtesanos.internal();
  factory FirebaseArtesanos() => _instance;
  FirebaseArtesanos.internal();
  Future<ArtModel> createArtesano(
      String name,
      String lastName,
      String direccion,
      int cell,
      String asociacion,
      String comunidad,
      String materiales,
      String descripcion) {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot doc = await tx.get(artColl.doc());
      final ArtModel prod = new ArtModel(doc.id, name, lastName, direccion,
          cell, asociacion, comunidad, materiales, descripcion);

      final Map<String, dynamic> data = prod.toMap();
      tx.set(doc.reference, data);
      return data;
    };
    return FirebaseFirestore.instance
        .runTransaction(createTransaction)
        .then((mapData) {
      return ArtModel.fromMap(mapData);
    }).catchError((e) {
      print('e:$e');
      return null;
    });
  }

  Stream<QuerySnapshot> getListProd({int offset, int limit}) {
    Stream<QuerySnapshot> snapShot = artColl.snapshots();
    if (offset != null) {
      snapShot = snapShot.skip(offset);
    }
    if (limit != null) {
      snapShot = snapShot.skip(limit);
    }
    return snapShot;
  }
}
