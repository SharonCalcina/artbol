import 'package:cloud_firestore/cloud_firestore.dart';
import 'prod_model.dart';
import 'dart:async';

final CollectionReference prodColl =
    FirebaseFirestore.instance.collection('productos');

class FirebaseService {
  static final FirebaseService _instance = new FirebaseService.internal();
  factory FirebaseService() => _instance;
  FirebaseService.internal();
  Future<ProdModel> createProd(
      String name, String image, String desc, int price, int quantity) {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot doc = await tx.get(prodColl.doc());
      final ProdModel prod =
          new ProdModel(doc.id, name, image, desc, price, quantity);
      final Map<String, dynamic> data = prod.toMap();
      tx.set(doc.reference, data);
      return data;
    };
    return FirebaseFirestore.instance
        .runTransaction(createTransaction)
        .then((mapData) {
      return ProdModel.fromMap(mapData);
    }).catchError((e) {
      print('e:$e');
      return null;
    });
  }

  Stream<QuerySnapshot> getListProd({int offset, int limit}) {
    Stream<QuerySnapshot> snapShot = prodColl.snapshots();
    if (offset != null) {
      snapShot = snapShot.skip(offset);
    }
    if (limit != null) {
      snapShot = snapShot.skip(limit);
    }
    return snapShot;
  }
}
