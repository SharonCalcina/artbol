import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:modal_progress_hud/modal_progress_hud.dart';

class CommonThings {
  static Size size;
}

class CrearProductos extends StatefulWidget {
  final String id;
  const CrearProductos({this.id});

  @override
  _CrearProductosState createState() => _CrearProductosState();
}

enum SelectSource { camara, galeria }

class _CrearProductosState extends State<CrearProductos> {
  File _foto;
  String urlFoto;
  bool _isInAsyncCall = false;
  int price;
  //Auth auth = Auth();

  TextEditingController priceInputController;
  TextEditingController nameInputController;
  TextEditingController imageInputController;
  TextEditingController descriptionInputController;
  int _selectedPage = 1;
  int _selected = 1;
  String id;
  final db = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();
  String name;
  String desc;
  String uid;
  bool showSelect = false;

  Future captureImage(SelectSource opcion) async {
    final picker = ImagePicker();
    PickedFile imagen;

    opcion == SelectSource.camara
        ? imagen = await picker.getImage(source: ImageSource.camera)
        : imagen = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _foto = File(imagen.path);
    });
  }

  Future getImage() async {
    AlertDialog alerta = new AlertDialog(
      content: Text('Seleccione de donde desea capturar la imagen'),
      title: Text('Seleccione la Imagen'),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            // seleccion = SelectSource.camara;
            captureImage(SelectSource.camara);
            Navigator.of(context, rootNavigator: true).pop();
          },
          child: Row(
            children: <Widget>[Text('Camara'), Icon(Icons.camera)],
          ),
        ),
        FlatButton(
          onPressed: () {
            // seleccion = SelectSource.galeria;
            captureImage(SelectSource.galeria);
            Navigator.of(context, rootNavigator: true).pop();
          },
          child: Row(
            children: <Widget>[Text('Galeria'), Icon(Icons.image)],
          ),
        )
      ],
    );
    showDialog(context: context, child: alerta);
  }

  Widget divider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
      child: Container(
        width: 0.8,
        color: Colors.black,
      ),
    );
  }

  bool _validarlo() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _enviar() {
    String page = '';
    String id;
    int m = 0;
    if (_validarlo()) {
      setState(() {
        _isInAsyncCall = true;
      });

      if (_foto != null) {
        if (_selectedPage == 1) {
          page = 'ceramica';
          id = 'a' + m.toString();
        }
        if (_selectedPage == 2) {
          page = 'productos';
          m = m + 1;
          id = 'b' + m.toString();
        }
        if (_selectedPage == 3 && _selected == 1) {
          page = 'ofertas';
          m = m + 1;
          id = 'c' + m.toString();
        }
        if (_selectedPage == 3 && _selected == 2) {
          page = 'ofertasRZA';
          m = m + 1;
          id = 'c' + m.toString();
        }
        if (_selectedPage == 3 && _selected == 3) {
          page = 'ofDecExtInt';
          m = m + 1;
          id = 'c' + m.toString();
        }
        if (_selectedPage == 3 && _selected == 4) {
          page = 'ofertasOtros';
          m = m + 1;
          id = 'c' + m.toString();
        }
        if (_selectedPage == 4) {
          page = 'pielCueroTextil';
          m = m + 1;
          id = 'd' + m.toString();
        }
        if (_selectedPage == 5) {
          page = 'marmolPiedraYesoVidrio';
          m = m + 1;
          id = 'e' + m.toString();
        }
        if (_selectedPage == 6) {
          page = 'metal';
          m = m + 1;
          id = 'f' + m.toString();
        }
        if (_selectedPage == 7) {
          page = 'joyeria';
          m = m + 1;
          id = 'g' + m.toString();
        }
        if (_selectedPage == 8) {
          page = 'instrumentos';
          m = m + 1;
          id = 'h' + m.toString();
        }
        if (_selectedPage == 9 && _selected == 1) {
          page = 'otros';
          m = m + 1;
          id = 'i' + m.toString();
        }
        if (_selectedPage == 9 && _selected == 2) {
          page = 'otrosRZA';
          m = m + 1;
          id = 'i' + m.toString();
        }
        if (_selectedPage == 9 && _selected == 3) {
          page = 'otrosDecExtInt';
          m = m + 1;
          id = 'i' + m.toString();
        }
        if (_selectedPage == 9 && _selected == 4) {
          page = 'otrosOtros';
          m = m + 1;
          id = 'i' + m.toString();
        }

        final StorageReference fireStoreRef =
            FirebaseStorage.instance.ref().child(page).child('$name.jpg');
        final StorageUploadTask task = fireStoreRef.putFile(
            _foto, StorageMetadata(contentType: 'image/jpeg'));
        task.onComplete.then((onValue) {
          onValue.ref.getDownloadURL().then((onValue) {
            setState(() {
              urlFoto = onValue.toString();
              FirebaseFirestore.instance
                  .collection(page)
                  .add({
                    'id': id,
                    'name': name,
                    'image': urlFoto,
                    'price': price,
                    'desc': desc,
                  })
                  .then((value) => Navigator.of(context).pop())
                  .catchError(
                      (onError) => print('Error al registrar su produtos bd'));
              _isInAsyncCall = false;
            });
          });
        });
      } else {
        FirebaseFirestore.instance
            .collection('productos')
            .add({
              'name': name,
              'image': urlFoto,
              'price': price,
              'desc': desc,
            })
            .then((value) => Navigator.of(context).pop())
            .catchError((onError) => print('Error al registrar su producto'));
        _isInAsyncCall = false;
      }
    } else {
      print('objeto no validado');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          flexibleSpace: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width / 6, top: 20),
                  child: Image(
                    image: AssetImage('assets/images/logo2.png'),
                    width: 150,
                    height: 80,
                  )),
              Spacer(),
            ],
          ),
        ),
        body: ModalProgressHUD(
          inAsyncCall: _isInAsyncCall,
          opacity: 0.5,
          dismissible: false,
          progressIndicator: CircularProgressIndicator(),
          color: Colors.blueGrey,
          child: SingleChildScrollView(
            padding: EdgeInsets.only(left: 10, right: 15),
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width - 40,
                        child: Container(
                          alignment: Alignment.center,
                          child: GestureDetector(
                            onTap: getImage,
                          ),
                          margin: EdgeInsets.only(
                              left: 20, right: 20, bottom: 20, top: 40),
                          height: 200,
                          width: 200,
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 2.5, color: Colors.green),
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: _foto == null
                                      ? AssetImage('assets/images/logo2.png')
                                      : FileImage(_foto))),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    'Haz click en la imagen para cambiar de foto',
                    style: TextStyle(
                        color: Colors.red.shade300,
                        fontFamily: 'Roboto',
                        fontStyle: FontStyle.italic,
                        fontSize: 12),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 15),
                      maxLines: 2,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'nombre',
                        //fillColor: Colors.grey[300],
                        filled: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingresa algún nombre';
                        }
                        return null;
                      },
                      onSaved: (value) => name = value.trim(),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 15),
                      maxLines: 3,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'decripción',
                        //fillColor: Colors.grey[300],
                        filled: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingresa alguna descripción';
                        }
                        return null;
                      },
                      onSaved: (value) => desc = value.trim(),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    child: TextFormField(
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Roboto',
                          fontSize: 15),
                      maxLines: 1,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'precio en Bs',
                        //fillColor: Colors.grey[300],
                        filled: true,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingresa algún precio';
                        }
                        return null;
                      },
                      onSaved: (value) => price = int.parse(value),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      'Selecciona la pagina para añadir tu producto (*)',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black.withOpacity(0.1),
                    ),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    margin: EdgeInsets.only(bottom: 10),
                    child: DropdownButton(
                        focusColor: Colors.blue,
                        iconDisabledColor: Colors.blue,
                        iconEnabledColor: Colors.red,
                        dropdownColor: Colors.grey.shade300,
                        //hint: Text(
                        //    'Selecciona la pagina del prod'),
                        value: _selectedPage,
                        items: [
                          DropdownMenuItem(
                            child: Text(
                              "Artesanias de ceramica",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                              textAlign: TextAlign.center,
                            ),
                            value: 1,
                          ),
                          DropdownMenuItem(
                              child: Text(
                                "Artesanias de madera",
                                style: TextStyle(
                                    fontFamily: 'Roboto', fontSize: 15),
                              ),
                              value: 2),
                          DropdownMenuItem(
                            child: Text(
                              "Artesanias de piel cuero y textil",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                            ),
                            value: 4,
                          ),
                          DropdownMenuItem(
                            child: Text(
                              "Artesanias de marmol, piedra, yeso y vidrio",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                            ),
                            value: 5,
                          ),
                          DropdownMenuItem(
                            child: Text(
                              "Artesanias de metal",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                            ),
                            value: 6,
                          ),
                          DropdownMenuItem(
                            child: Text(
                              "Joyeria",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                            ),
                            value: 7,
                          ),
                          DropdownMenuItem(
                            child: Text(
                              "Instrumentos Musicales",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                            ),
                            value: 8,
                          ),
                          DropdownMenuItem(
                            child: Text(
                              "Otros tipos de artesania",
                              style:
                                  TextStyle(fontFamily: 'Roboto', fontSize: 15),
                            ),
                            value: 9,
                          ),
                          DropdownMenuItem(
                              child: Text(
                                "Ofertas",
                                style: TextStyle(
                                    fontFamily: 'Roboto', fontSize: 15),
                              ),
                              value: 3),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _selectedPage = value;
                            if (_selectedPage == 9 || _selectedPage == 3) {
                              showSelect = true;
                            }
                          });
                        }),
                  ),
                  Visibility(
                      visible: showSelect,
                      child: Container(
                        margin: EdgeInsets.all(5),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'Selecciona el area de aplicacion de tu producto (*)',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 15,
                              fontStyle: FontStyle.italic),
                        ),
                      )),
                  Visibility(
                    visible: showSelect,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.black.withOpacity(0.1),
                      ),
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(left: 15, right: 15),
                      margin: EdgeInsets.only(bottom: 15),
                      child: DropdownButton(
                          hint: Text('Selecciona el area del producto'),
                          focusColor: Colors.blue,
                          iconDisabledColor: Colors.blue,
                          iconEnabledColor: Colors.red,
                          dropdownColor: Colors.grey.shade300,
                          value: _selected,
                          items: [
                            DropdownMenuItem(
                              child: Text(
                                "Cocina",
                                style: TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 15,
                                ),
                              ),
                              value: 1,
                            ),
                            DropdownMenuItem(
                                child: Text(
                                  "Ropa, zapatos y accesorios",
                                  style: TextStyle(
                                      fontFamily: 'Roboto', fontSize: 15),
                                ),
                                value: 2),
                            DropdownMenuItem(
                                child: Text(
                                  "Decorado interiores y/o exteriores",
                                  style: TextStyle(
                                      fontFamily: 'Roboto', fontSize: 15),
                                ),
                                value: 3),
                            DropdownMenuItem(
                              child: Text(
                                "Otros",
                                style: TextStyle(
                                    fontFamily: 'Roboto', fontSize: 15),
                              ),
                              value: 4,
                            ),
                          ],
                          onChanged: (value) {
                            setState(() {
                              _selected = value;
                            });
                          }),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: _enviar,
                        child: Text('Crear Producto',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Roboto',
                                fontSize: 15)),
                        color: Colors.green,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
