import 'package:artbol/pages/Help.dart';
import 'package:artbol/pages/Home.dart';
import 'package:artbol/pagesSubmenu/Offers.dart';
import 'package:artbol/pages/ProdsCatalog.dart';
import 'package:artbol/pages/Sell.dart';
import 'package:artbol/side_bar/Register.dart';
import 'package:artbol/side_bar/States.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'side_bar/States.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => UserRepository.instance(),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                scaffoldBackgroundColor: Colors.white,
                primaryColor: Colors.black),
            initialRoute: 'home',
            routes: {
              'home': (_) => HomePage(
                    text: 'home',
                  ),
              ProdCatalog.routeProd: (_) => ProdCatalog(),
              HomePage.routeHome: (_) => HomePage(
                    text: 'home',
                  ),
              Offers.routeOf: (_) => Offers(),
              Sells.routeSell: (_) => Sells(),
              Help.routeHelp: (_) => Help(),
              Register.routeReg: (_) => Register(
                    text: 'register',
                  ),
            }));
  }
}
